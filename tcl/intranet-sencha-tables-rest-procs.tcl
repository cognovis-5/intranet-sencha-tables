# /packages/intranet-sencha-tables-rest-procs

ad_library {
	Rest Procedures for the sencha-tables package
	
	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_rest_get_custom_sencha_company {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for GET rest calls on companies.
	Endpoint is using dynfields
} {
	set rest_otype "im_company"
	ns_log Notice "im_rest_get_sencha_companies: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
	array set query_hash $query_hash_pairs
	set base_url "[im_rest_system_url]/cognovis-rest"

	if {"" != $rest_oid} { set query_hash(company_id) $rest_oid }


	set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_company'" -default 0]]
	set rest_otype_read_all_p [im_permission $rest_user_id "view_companies_all"]
    
	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	if {[info exists query_hash(deref_p)]} {
		set deref_p $query_hash(deref_p)
	} else {
		set deref_p 0
	}

	# ---------------------------------------------------------------
	# Valid vars combined from company office and person
	# ---------------------------------------------------------------

	# These are variables which can't be handled in dynfields as they are used in joins
	# Or result of  a coalesce
	set valid_vars [list company_id primary_contact_phone]
	set select_vars [list im_companies.primary_contact_id im_companies.company_id "coalesce(home_phone,work_phone,cell_phone) as primary_contact_phone"]
	set where_clauses [list "im_companies.main_office_id = im_offices.office_id" "im_companies.primary_contact_id = persons.person_id"]

	# Append the variables. As we deal with dynfields all attributes should have a deref function
	set company_var_info [im_dynfield::sorted_attributes_selects -object_type im_company -page_url "/cognovis-rest/sencha_company" -deref_p $deref_p]

	set valid_vars [concat $valid_vars [lindex $company_var_info 0]]
	set select_vars [concat $select_vars [lindex $company_var_info 1]]
	
	set tables [lindex $company_var_info 2]
	set outer_joins [lindex $company_var_info 3]
	
	set from_sql "from [join $tables ","] [join $outer_joins " "]"
	
	set where_clauses [concat $where_clauses [lindex $company_var_info 4]]
	
	# Now append the offices and person information
	# Thiis is tricky due to the possible outer joins and multiple tables showing up multiple times (im_biz_objects)
	
	# Office variables
	set office_var_info [im_dynfield::sorted_attributes_selects -object_type im_office -page_url "/cognovis-rest/sencha_company" -deref_p $deref_p -exclude_tables $tables]
	set valid_vars [concat $valid_vars [lindex $office_var_info 0]]
	set select_vars [concat $select_vars [lindex $office_var_info 1]]
	set office_tables [list]
	foreach table [lindex $office_var_info 2] {
		if {[lsearch $tables $table]<0} {
			lappend tables $table
			lappend office_tables $table
		}
	}
	set from_sql "$from_sql , [join $office_tables ","] [join [lindex $office_var_info 3] " "]"
	
	set where_clauses [concat $where_clauses [lindex $office_var_info 4]]

	# person variables
	set person_var_info [im_dynfield::sorted_attributes_selects -object_type person -page_url "/cognovis-rest/sencha_company" -deref_p $deref_p -exclude_tables $tables]
	set valid_vars [concat $valid_vars [lindex $person_var_info 0]]
	set select_vars [concat $select_vars [lindex $person_var_info 1]]
	set person_tables [list]
	foreach table [lindex $person_var_info 2] {
		if {[lsearch $tables $table]<0} {
			lappend tables $table
			lappend person_tables $table
		}
	}
	set from_sql "$from_sql , [join $person_tables ","] [join [lindex $person_var_info 3] " "]"
	
	set where_clauses [concat $where_clauses [lindex $person_var_info 4]]
	

		
	# -------------------------------------------------------
	# Check if there is a where clause specified in the URL and validate the clause.
	set where_clause ""
	if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}

	# ---------------------------------------------------------------
	# Check for categories and sub-categories
	# We only have special ones here.
	# ---------------------------------------------------------------

	set multi_filters {company_status_id company_type_id}
	set where_clause_list $where_clauses
	    
	foreach v $multi_filters {
		if {[info exists query_hash($v)]} {
		    set values $query_hash($v)
		    set filter_ids [list]
		    foreach value_id [split $values ","] {
		    	if {[im_sub_categories $value_id] eq 0} {
                    set filter_ids [concat $filter_ids $value_id]
		        } else {
                    set filter_ids [concat $filter_ids [im_sub_categories $value_id]]
		    	}	    
		    }
		    set where_clause_v "$v in ([template::util::tcl_to_sql_list $filter_ids])"
		    lappend where_clause_list $where_clause_v
		}
	}


	# -------------------------------------------------------
	# Check if there are "valid_vars" specified in the HTTP header
	# and add these vars to the SQL clause

	#ns_log Debug "Valid:: $valid_vars ... [array get query_hash]"
	foreach v $valid_vars {
		if {[info exists query_hash($v)] && [lsearch $multi_filters $v]<0} {
			set value $query_hash($v)
			
			# deal with variables showing up multiple time
			switch $v {
				company_id {
					set where_clause_v "im_companies.company_id = $value"
				}
				primary_contact_id {
					set where_clause_v "im_companies.primary_contact_id = $value"
				}
				default {
					where_clause_v "$v=$value"
				}
			}
			lappend where_clause_list $where_clause_v
		}
	}

	# ----------------------------------------------------------------------
	# If we want to minimize size of returned JSON using 'first 3 letters'
	# --------------------------------------------------------------------
	set autocomplete_query_p 0
	if {[exists_and_not_null query_hash(autocomplete_query)]} {
		set autocomplete_query_value $query_hash(autocomplete_query)
		if {$autocomplete_query_value ne ""} {
			set autocomplete_query_p 1
			lappend where_clause_list "lower(company_name) like lower('$autocomplete_query_value%')"
		}
	}
		
	# Append the where clause list to the where clause
	if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
	append where_clause [join $where_clause_list " and "]

	# Check that the query is a valid SQL where clause
	set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
	if {!$valid_sql_where} {
		im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"
		return
	}

#set where_clause_list [lreplace $where_clause_list 1 1]

	set sql "select [join $select_vars ","] \
		$from_sql \
		[join $outer_joins " "] \
		where [join $where_clause_list " and "]"


	# ---------------------------------------------------------------
	# If we filter by „to contact“ show the invoices
	# ---------------------------------------------------------------
	set with_invoices_p 0
	if {[info exists query_hash(company_status_id)]} {
		if {$query_hash(company_status_id) eq 4800} {
			set with_invoices_p 1
		}
	}
	
	
	if {$with_invoices_p} {
		lappend valid_vars "total_invoice_amount"
		lappend valid_vars "total_quote_amount"
		
		db_foreach invoices "select customer_id,sum(amount) as invoice_amount from im_costs where cost_status_id not in (3812,3813,3814,3816,3818) and cost_type_id = [im_cost_type_invoice] group by customer_id" {
			set total_invoice($customer_id) $invoice_amount
		}
		db_foreach quotes "select customer_id,sum(amount) as quote_amount from im_costs where cost_status_id not in (3812,3813,3814,3816,3818) and cost_type_id = [im_cost_type_quote] group by customer_id" {
			set total_quote($customer_id) $quote_amount
		}
	}
	
	
	# ---------------------------------------------------------------
	# Get company members
	# ---------------------------------------------------------------

	set with_members_p 0
	if {[info exists query_hash(members_p)] && $query_hash(members_p) eq 1} {
		set with_members_p 1
	}
	
	if {$with_members_p} {
		set inactive_company_status_ids [im_sub_categories [im_company_status_inactive]]
		set member_sql "
			select company_id, object_id_two as member_id, im_name_from_id(object_id_two) as member_name
			from im_companies c, acs_rels r
			where company_id not in ([template::util::tcl_to_sql_list $inactive_company_status_ids])
			$where_clause
			and r.object_id_one = c.company_id
			and r.object_id_two not in (
				-- Exclude banned or deleted users
				select	m.member_id
				from	group_member_map m,
					membership_rels mr
				where	m.rel_id = mr.rel_id and
					m.group_id = acs__magic_object_id('registered_users') and
					m.container_id = m.group_id and
					mr.member_state != 'approved'
			)
			and r.object_id_two not in (
				select	gdmm.member_id
				from	group_distinct_member_map gdmm
				where	gdmm.group_id = 463
			)
		"
	
		db_foreach member $member_sql {
			if {![info exists member_js($company_id)]} {
			    set member_js($company_id) [list "\{ \"member_id\": \"$member_id\", \"member_name\": \"[im_quotejson $member_name]\" \}"]
			} else {
			    lappend member_js($company_id) "\{ \"member_id\": \"$member_id\",\"member_name\": \"[im_quotejson $member_name]\" \}"
			}
		}
	}
	
	# ---------------------------------------------------------------
	# Get company notes
	# ---------------------------------------------------------------
	
	set with_notes_p 0
	if {[info exists query_hash(notes_p)] && $query_hash(notes_p) eq 1} {
		set with_notes_p 1
	}

	if {$with_notes_p} {
		set note_company_ids [list]
		db_foreach companies $sql {
			if {$primary_contact_id ne ""} {
				set note_object_arr($company_id) [list $company_id $primary_contact_id]
			} else {
				set note_object_arr($company_id) $company_id
			}
			lappend note_company_ids $company_id
		}
		
		foreach company_id $note_company_ids {
			set note_company_ids $note_object_arr($company_id)
			
			db_foreach notes "select note_id, note, note_type_id from im_notes where object_id in ([template::util::tcl_to_sql_list $note_company_ids])" {
				set note [template::util::richtext::get_property html_value $note]
				set note_teaser [string range [ad_html_text_convert -from "text/html" -to "text/plain" -- $note] 0 40]
				if {![info exists notes_js($company_id)]} {
				set notes_js($company_id) [list "\{ \"note_id\": \"$note_id\", \"note\": \"[im_quotejson $note]\", \"note_teaser\": \"[im_quotejson $note_teaser]\", \"note_type_id\": \"$note_type_id\" \}"]
				} else {
				    lappend notes_js($company_id) "\{ \"note_id\": \"$note_id\", \"note\": \"[im_quotejson $note]\", \"note_teaser\": \"[im_quotejson $note_teaser]\", \"note_type_id\": \"$note_type_id\" \}"
				}
			}
		}
	}

	
	
	# Append pagination "LIMIT $limit OFFSET $start" to the sql.
	set unlimited_sql $sql
	#append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

	set value ""
	set result ""
	set obj_ctr 0
  
    # Handler for country (as we need country and not country code)
	lappend valid_vars address_country_full
    set country_codes [im_country_options]
	
        db_foreach objects $sql {
            
            set address_country_full ""
            foreach country $country_codes {
                set country_code_ch [lindex $country 1]
                set country_name_ch [lindex $country 0]
                if {$address_country_code eq $country_code_ch} {
                    set address_country_full $country_name_ch
                }
            }

		if {[info exists total_invoice($company_id)] && $with_invoices_p} {
			set total_invoice_amount $total_invoice($company_id)
		} else {
			set total_invoice_amount ""
		}

		if {[info exists total_quote($company_id)] && $with_invoices_p} {
			set total_quote_amount $total_quote($company_id)
		} else {
			set total_quote_amount ""
		}
		
		# Check permissions, even if user do not have permission we still check if he is not asking for his own company. 
		# If yes, then we can display that single company to him\
        # We check if rest_user_id is company_id member
	    set rest_user_is_company_member_p [db_string rest_user_is_company_member "select 1 from acs_rels where object_id_one =:company_id and object_id_two =:rest_user_id and rel_type='im_company_employee_rel'" -default 0]


		set read_p $rest_otype_read_all_p
		if {!$read_p && !$rest_user_is_company_member_p} { continue }



		set url "$base_url/$rest_otype/$rest_oid"
		switch $format {
			html {
			append result "<tr>
				<td>$rest_oid</td>
				<td><a href=\"$url?format=html\">$company_name</a></td>
				<td>$primary_contact_name</td>
			</tr>\n"
			}
			json {
				set komma ",\n"
				if {0 == $obj_ctr} { set komma "" }
				set dereferenced_result ""
				foreach v $valid_vars {

					eval "set a $$v"
					regsub -all {\n} $a {} a
					regsub -all {\r} $a {} a
					append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
				}
				
				if {$with_members_p} {
					if {[info exists member_js($company_id)]} {
						append dereferenced_result ", \"members\": \[ [join $member_js($company_id) ", \n"] \]"
					} else {
						append dereferenced_result ", \"members\": \"\""
					}
				}
				if {$with_notes_p} {
					if {[info exists notes_js($company_id)]} {
						append dereferenced_result ", \"notes\": \[ [join $notes_js($company_id) ", \n"] \]"
					} else {
						append dereferenced_result ", \"notes\": \"\""
					}
				}
				append result "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[im_quotejson $company_name]\"$dereferenced_result}"
			}
			default {}
		}
		incr obj_ctr
	}

	switch $format {
		html {
			set page_title "object_type: $rest_otype"
			im_rest_doc_return 200 "text/html" "
			[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
			<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
			</table>[im_footer]
			"
		}
		json {
			set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_im_categories: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
			im_rest_doc_return 200 "application/json" $result
			return
		}
	}
	return
}

ad_proc -public im_rest_get_custom_sencha_project {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for GET rest calls on invoice items.
} {
	set rest_otype "im_project"
    	array set query_hash $query_hash_pairs
	set base_url "[im_rest_system_url]/cognovis-rest"

    ns_log Notice "im_rest_get_sencha_project: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=[array get query_hash]"

	if {"" != $rest_oid} { set query_hash(project_id) $rest_oid }


	set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_project'" -default 0]]
	set rest_otype_read_all_p [im_permission $rest_user_id "view_projects_all"]

	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	# -------------------------------------------------------
	# Valid variables to return for im_company
	set valid_vars {project_id project_name project_nr company_id company_name percent_completed project_lead_id project_manager project_status_id project_type_id start_date end_date subject_area_id source_language_id expected_quality_id}

	# -------------------------------------------------------
	# Check if there is a where clause specified in the URL and validate the clause.
	set where_clause ""
	if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}


	# -------------------------------------------------------
	# Check if there are "valid_vars" specified in the HTTP header
	# and add these vars to the SQL clause
	set where_clause_list [list]
	foreach v $valid_vars {
		if {[info exists query_hash($v)]} {
			set value_list [split $query_hash($v) ","]
			if {$v eq "company_id"} {
				set v "p.company_id"
			}
			lappend where_clause_list "$v in ([template::util::tcl_to_sql_list $value_list])" }
	}
	if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
	append where_clause [join $where_clause_list " and "]


	# Check that the query is a valid SQL where clause
	set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
	if {!$valid_sql_where} {
	im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"
	return
	}
	if {"" != $where_clause} { set where_clause "and $where_clause" }


    # Limit the date range if no company is provided
    if {![info exists query_hash(company_id)]} {
	append where_clause "and end_date > now() -interval '6 months'"
    }

	# Select SQL: Pull out categories.
	set sql "
		select project_id, project_name, project_nr, c.company_id, c.company_name, project_status_id, project_type_id, first_names || ' ' || last_name as project_manager, project_lead_id, percent_completed, p.start_date, p.end_date, p.subject_area_id, p.source_language_id, p.expected_quality_id
		from im_projects p, im_companies c, persons pe
                where c.company_id = p.company_id
		and project_type_id not in (100,101)
                and pe.person_id = p.project_lead_id
		$where_clause
	order by project_name
	"

	# Append pagination "LIMIT $limit OFFSET $start" to the sql.
	set unlimited_sql $sql
	append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

	set value ""
	set result ""
	set obj_ctr 0
	
	# ---------------------------------------------------------------
	# Get project members
	# ---------------------------------------------------------------
	set with_members_p 0
	if {[info exists query_hash(members_p)] && $query_hash(members_p) eq 1} {
		set with_members_p 1
	}
	
	if {$with_members_p} {

	    set member_sql "
		select project_id, object_id_two as freelancer_id, im_name_from_id(object_id_two) as freelancer_name
		from im_projects p, acs_rels r, group_distinct_member_map m
		where project_type_id not in (100,101)
		$where_clause
		and r.object_id_one = p.project_id
		and r.object_id_two = m.member_id
		and m.group_id = 465
            "

	    db_foreach member $member_sql {
		if {![info exists member_js($project_id)]} {
		    set member_js($project_id) [list "\{ \"freelancer_id\": \"$freelancer_id\", \"freelancer_name\": \"[im_quotejson $freelancer_name]\" \}"]
		} else {
		    lappend member_js($project_id) "\{ \"freelancer_id\": \"$freelancer_id\", \"freelancer_name\": \"[im_quotejson $freelancer_name]\" \}"
		}
	    }
	}

	set target_language_ids "[]"
    set language_experience_level_id ""
    #quering for single project ?
    set single_project_query_p 0
	if {[exists_and_not_null query_hash(project_id)]} {
		set single_project_query_p 1
        set queried_project_id $query_hash(project_id)
	}

    if {$single_project_query_p == 1} {
    	set count_target_languages 0
	    set target_language_id_sql "select language_id as lang from im_target_languages where project_id = :queried_project_id"
	    db_foreach lang $target_language_id_sql {
	    	if {$count_target_languages > 0} {
	    		append target_language_ids ",$lang"
	    	} else {
	    		append target_language_ids "$lang"
	    	}
            incr count_target_languages
	    }
	    if {[apm_package_installed_p "intranet-freelance"]} {
           set language_experience_level_id [db_string language_experience_level_query "select skill_id from im_object_freelance_skill_map where object_id =:queried_project_id AND skill_type_id = 2028" -default ""]
	    }
	}
	
	db_foreach objects $sql {

		# Check permissions
		set read_p $rest_otype_read_all_p
		if {!$read_p} { continue }


		set url "$base_url/$rest_otype/$project_id"

		switch $format {
			html {
			append result "<tr>
				<td>$rest_oid</td>
				<td><a href=\"$url?format=html\">$project_name</a></td>
				<td>$project_manager</td>
			</tr>\n"
			}
		json {
			set komma ",\n"
			if {0 == $obj_ctr} { set komma "" }
			set dereferenced_result ""
			foreach v $valid_vars {
				eval "set a $$v"
				regsub -all {\n} $a {\n} a
				regsub -all {\r} $a {} a
				append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
			}
           set project_words 0
           set target_language_ids [list]
           set target_language_names [list]
           set sql "
               select
	           l.language_id as target_language_id, im_category_from_id(l.language_id) as target_language_name
               from
	           im_target_languages l
               where
	           project_id=:project_id
           "

            db_foreach select_target_languages $sql {
	            lappend target_language_ids $target_language_id
	            lappend target_language_names $target_language_name
            }
    
			set first_target_language_id [lindex $target_language_ids 0]
			if {$first_target_language_id ne ""} {
			    db_0or1row project_words "select sum(task_units) as project_words from im_trans_tasks where target_language_id =:first_target_language_id and project_id =:project_id and task_status_id <> 372"
			}

			set target_language_names_comma [join $target_language_names ", "]
			
			if {[info exists member_js($project_id)]} {
				append dereferenced_result ", \"members\": \[ [join $member_js($project_id) ", \n"] \]"
			} else {
				append dereferenced_result ", \"members\": \"\""
			}
			if {$single_project_query_p == 1} {
			    append result "$komma{\"id\": \"$project_id\", \"language_experience_level_id\": \"$language_experience_level_id\", \"target_language_ids\": \"$target_language_ids\", \"object_name\": \"[im_quotejson $project_name]\"$dereferenced_result}"
			} else {
                append result "$komma{\"id\": \"$project_id\", \"project_words\": \"$project_words\", \"target_language_ids\": \"$target_language_ids\", \"target_language_names\": \"$target_language_names\", \"target_language_names_comma\": \"$target_language_names_comma\", \"object_name\": \"[im_quotejson $project_name]\"$dereferenced_result}"
			}
		}
		default {}
	}
		incr obj_ctr
	}

	switch $format {
		html {
			set page_title "object_type: $rest_otype"
			im_rest_doc_return 200 "text/html" "
			[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
			<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
			</table>[im_footer]
			"
		}
		json {
			return "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_sencha_project: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
		}
	}
}

ad_proc -public im_rest_get_custom_sencha_menus {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler to return the menu items of a parent menu / label
} {
	array set query_hash $query_hash_pairs
	
	if {[info exists query_hash(parent_menu_id)]} {
		set parent_menu_id $query_hash(parent_menu_id)
	} elseif {[info exists query_hash(label)]} {
		set label $query_hash(label)
		set parent_menu_id [db_string label "select menu_id from im_menus where label = :label"]
	} else {
		# We can't work without a project!
		im_rest_error -format "json" -http_status 403 -message "Missing a parent_menu_id or label"
	}


	set result ""
	set obj_ctr 0
	set valid_vars [list name url action_type sort_order]
	set locale [lang::user::locale -user_id $rest_user_id]
	set parent_url [db_string url "select url from im_menus where menu_id = :parent_menu_id" -default ""]
	set parent_base_url [lindex [split $parent_url "?"] 0]
	
	if {"" == $locale} { set locale [lang::user::locale -user_id $rest_user_id] }
	
	set menu_select_sql "
		select	menu_id,
			package_name,
			label,
			name,
			url,
			visible_tcl,
			sort_order
		from	im_menus m
		where	parent_menu_id = :parent_menu_id
			and (enabled_p is null OR enabled_p = 't')
			and im_object_permission_p(m.menu_id, :rest_user_id, 'read') = 't'
	"
	
	db_foreach subnavbar_menus $menu_select_sql {
		set name_key "intranet-core.[lang::util::suggest_key $name]"
		set name [lang::message::lookup "" $name_key $name]

		#ns_log Debug "NAME:: $name --- $visible_tcl"
		set komma ",\n"
		set dereferenced_result ""
		if {0 == $obj_ctr} { set komma "" }
		
		if {"" != $visible_tcl} {
			# Interpret empty visible_tcl menus as always visible
			
			set errmsg ""
			set visible 0
			if [catch {
				set visible [expr $visible_tcl]
			} errmsg] {
				ns_log Error "im_rest_get_custom_sencha_menus: <pre>$errmsg</pre>"
			}
					
			if {!$visible} { continue }
		}

		# Action Type: If the parent url is the same as the url (bar the filters)
		# then set the action type to filter avoiding a reload of the page
		# Otherwise set action_type to refresh
		set base_url [lindex [split $url "?"] 0]
		if {$base_url eq $parent_base_url} {
			set action_type "filter"
		} else {
			set action_type "refresh"
		}

		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}

		append result "$komma{\"id\": \"$menu_id\", \"object_name\": \"[im_quotejson $label]\"$dereferenced_result}"
		incr obj_ctr
	}
		
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_sencha_tables_rest_call: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}


ad_proc -public im_rest_get_custom_single_value_tcl {
	-tcl_proc
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Provide a single value back in JSON based on the tcl_proc being called
	
	Call this with /cognovis-rest/single_value_tcl?tcl_proc=im_next_project_nr&company_id=8711
	
	So you can pass on all arguments as URL values

	@param tcl_proc Procedure to call.

} {
	array set query_hash $query_hash_pairs

	if {[info exists query_hash(tcl_proc)]} {
		set tcl_proc $query_hash(tcl_proc)
	} else {
		# We can't work without a project!
		im_rest_error -format "json" -http_status 403 -message "Missing a tcl_proc to start with"
	}

	set valid_procs [info procs im_*_component]

	lappend valid_procs im_next_project_nr
	
	if {[lsearch $valid_procs $tcl_proc]<0} {
		im_rest_error -format "json" -http_status 403 -message "$tcl_proc is not a procedure we support yet"
	}
	
	# Check the attributes
	array set doc_elements [nsv_get api_proc_doc $tcl_proc]
	set cmd "$tcl_proc"
	foreach switch $doc_elements(switches) {
		if {[info exists query_hash($switch)]} {
			append cmd " -$switch $query_hash($switch)"
		}
	}

	if {[catch $cmd result]} {
		im_rest_error -format "json" -http_status 403 -message "Problem calling $cmd ::: $result"
	}
	
    set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"im_sencha_custom_single_value_tcl ($cmd): Data loaded\",\n\"data\": \[ \n \{ \"$tcl_proc\": \"[im_quotejson $result]\" \} \n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_rest_get_custom_sencha_biz_object_members {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler to return the Full Members of company (with @param object_one_id)
	It is used after we add new company and we need to select company primary contact

} {
	set rest_otype "im_biz_object_member"
	ns_log Notice "im_rest_get_sencha_companies: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
	array set query_hash $query_hash_pairs
	set base_url "[im_rest_system_url]/cognovis-rest"

	if {[info exists query_hash(object_id_one)]} {
		set object_id_one $query_hash(object_id_one)
	} else {
		im_rest_error -format "json" -http_status 403 -message "Missing an object_id_one to start with"
	}

	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	set valid_vars [list member_id member_name object_role_id role percentage]
	
	# Fix to exclude Key Accounts from query
	# This will require changing as sencha_biz_object_members should be more dynamic
	# Also in future we might check for permissions here
	set key_account_role_id [im_company_role_key_account]
	set dont_allow_group_id [im_employee_group_id]
	set member_sql "
		select
		rels.rel_id,
		rels.object_id_two as member_id, 
		rels.object_id_two as party_id, 
		im_email_from_user_id(rels.object_id_two) as email,
		im_name_from_id(rels.object_id_two) as member_name,
		im_name_from_id(bo_rels.object_role_id) as role,
		bo_rels.percentage,
		bo_rels.object_role_id
	from
		acs_rels rels
		LEFT OUTER JOIN im_biz_object_members bo_rels ON (rels.rel_id = bo_rels.rel_id)
		LEFT OUTER JOIN im_categories c ON (c.category_id = bo_rels.object_role_id)
	where
		rels.object_id_one = :object_id_one and
		rels.object_id_two in (select party_id from parties) and
		rels.object_id_two not in (
			-- Exclude banned or deleted users
			select	m.member_id
			from	group_member_map m,
				membership_rels mr
			where	m.rel_id = mr.rel_id and
				m.group_id = acs__magic_object_id('registered_users') and
				m.container_id = m.group_id and
				mr.member_state != 'approved'
		)
		and rels.object_id_two not in (
		    select	gdmm.member_id
		    from	group_distinct_member_map gdmm
		    where	gdmm.group_id = :dont_allow_group_id
	    ) 
	    "
		
    

	set value ""
	set result ""
	set obj_ctr 0
	db_foreach objects $member_sql {

		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}

		append result "$komma{\"id\": \"$rel_id\", \"object_name\": \"$object_id_one => $member_id\"$dereferenced_result}"
		incr obj_ctr
	}

	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_sencha_biz_object_member: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}



ad_proc -public im_rest_get_custom_group_members {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Get the list of members of a group
} {
	array set query_hash $query_hash_pairs
	
	set with_skills_p 0
	if {[info exists query_hash(skills_p)] && $query_hash(skills_p) eq 1} {
		set with_skills_p 1
	}
	
	set required_vars [list group_id]
   	foreach var $required_vars {
   		if {![info exists query_hash($var)]} {
   			return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
   		} else {
   			set $var $query_hash($var)
   		}
   	}


	set sql "select member_id, im_name_from_id(member_id) as member_name from group_member_map g where group_id = $group_id and g.member_id not in (
			-- Exclude banned or deleted users
			select	m.member_id
			from	group_member_map m,
				membership_rels mr
			where	m.rel_id = mr.rel_id and
				m.group_id = acs__magic_object_id('registered_users') and
				m.container_id = m.group_id and
				mr.member_state != 'approved'
		)
			"
			
	
	if {$with_skills_p} {
		# ---------------------------------------------------------------
		# Setup skill types and skills
		# ---------------------------------------------------------------
		
		set skill_type_ids [list]
		db_foreach skill_types "select category_id as skill_type_id, aux_string2 as variable_name from im_categories where category_type = 'Intranet Skill Type' and enabled_p = 't'" {
			lappend skill_type_ids $skill_type_id
			set var_arr($skill_type_id) $variable_name
		}
		
		
		db_foreach skills {select skill_type_id,user_id,skill_id,im_name_from_id(skill_id) as skill
			from im_freelance_skills
		} {
			if {![info exists skill_${skill_type_id}_js($user_id)]} {
				set skill_${skill_type_id}_js($user_id) [list "\{ \"skill_id\": \"$skill_id\", \"skill_name\": \"[im_quotejson $skill]\" \}"]
			} else {
				lappend skill_${skill_type_id}_js($user_id) "\{ \"skill_id\": \"$skill_id\", \"skill_name\": \"[im_quotejson $skill]\" \}"
			}
		}

	}

	set obj_ctr 0
	set valid_vars [list member_id member_name]
	db_foreach objects $sql {
		
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
	
		if {$with_skills_p} {
			foreach skill_type_id $skill_type_ids {
				if {$var_arr($skill_type_id) eq ""} {
					set var "skill_$skill_type_id"
				} else {
					set var "$var_arr($skill_type_id)"
				}
				if {[info exists skill_${skill_type_id}_js($member_id)]} {
					append dereferenced_result ", \n\"$var\": \[ [join [set skill_${skill_type_id}_js($member_id)] ", \n "] \]"
				} else {
					append dereferenced_result ", \n\"$var\": \"\""
				}
			}
		}
		
		if {$member_name ne ""} {
			append result "$komma{\"id\": \"$member_id\", \"object_name\": \"$member_name\"$dereferenced_result}"
		}
		incr obj_ctr
	}
	
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_sencha_biz_object_member: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_rest_get_custom_freelance_skills {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Get the list of skills a freelancer has for a given skill_type_id
} {
	array set query_hash $query_hash_pairs

	set required_vars [list freelance_id skill_type_id]
   	foreach var $required_vars {
   		if {![info exists query_hash($var)]} {
   			return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
   		} else {
   			set $var $query_hash($var)
   		}
   	}

	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	set obj_ctr 0
	set valid_vars [list skill_id skill_translated]

	db_foreach skills {select skill_id,category from im_freelance_skills fs, im_categories c where user_id = :freelance_id and skill_type_id = :skill_type_id and c.category_id = fs.skill_id and c.enabled_p = 't'} {

		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		
		set category_key "intranet-core.[lang::util::suggest_key $category]"
		set skill_translated [lang::message::lookup $locale $category_key $category]

		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}

		append result "$komma{\"id\": \"$skill_id\", \"object_name\": \"$category\"$dereferenced_result}"
		incr obj_ctr
	}

	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_freelance_skills: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_rest_get_custom_new_project_defaults {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Get the most often used values for the creation of a new project
} {
        
	array set query_hash $query_hash_pairs

	set required_vars [list customer_id]
   	foreach var $required_vars {
   		if {![info exists query_hash($var)]} {
   			return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
   		} else {
   			set $var $query_hash($var)
   		}
   	}

	 # Get the most often used project type for the customer
	 set project_type_id [db_string project_type "select project_type_id from (select project_type_id, count(project_type_id) as count from im_projects where company_id = :customer_id group by project_type_id) c order by count desc limit 1" -default ""]
	 set source_language_id [db_string source_langauge "select source_language_id from (select source_language_id, count(source_language_id) as count from im_projects where company_id = :customer_id group by source_language_id) c order by count desc limit 1" -default ""]
	 set subject_area_id [db_string subject_area "select subject_area_id from (select subject_area_id, count(subject_area_id) as count from im_projects where company_id = :customer_id group by subject_area_id) c order by count desc limit 1" -default ""]
	 set final_company_id [db_string final_company "select final_company_id from (select final_company_id, count(final_company_id) as count from im_projects where company_id = :customer_id group by final_company_id) c order by count desc limit 1" -default ""]
	 set target_language_id [db_string target_language "select language_id from (select language_id, count(language_id) as count from im_target_languages tl, im_projects p where p.company_id = :customer_id and p.project_id = tl.project_id group by language_id) c order by count desc limit 1" -default ""]
     set uom_id [db_string uom "select item_uom_id from (select count(item_uom_id) as count, item_uom_id from im_invoice_items ii, im_costs c where ii.invoice_id = c.cost_id and c.customer_id = :customer_id group by item_uom_id order by count desc) s limit 1" -default ""]

        set uom_id [db_string uom "select item_uom_id from (select count(item_uom_id) as count, item_uom_id from im_invoice_items ii, im_costs c where ii.invoice_id = c.cost_id and c.customer_id = :customer_id group by item_uom_id order by count desc) s limit 1" -default ""]
                
	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]
	set obj_ctr 1
        set target_language_ids $target_language_id
 	set valid_vars [list project_type_id source_language_id subject_area_id final_company_id target_language_id target_language_ids uom_id]

	set result "{\"id\": \"$customer_id\", \"object_name\": \"[im_quotejson [im_name_from_id $customer_id]]\""

	foreach v $valid_vars {
		eval "set a $$v"
		regsub -all {\n} $a {\n} a
		regsub -all {\r} $a {} a
		append result ", \"$v\": \"[im_quotejson $a]\""
	}
	append result "}"

	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_new_project_defaults: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_rest_get_custom_sencha_invoices {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for GET rest calls on invoice items.
} {
	
	array set query_hash $query_hash_pairs
	
	if {![info exists query_hash(cost_type_id)]} {
		set query_hash(cost_type_id) [im_cost_type_invoice]
	}

	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	# -------------------------------------------------------
	# Valid variables to return for im_company
	set valid_vars [list invoice_id customer_id customer_name company_contact_id company_contact_name company_contact_phone company_contact_email invoice_nr cost_status_id cost_status_pretty effective_date contact_again_date amount currency cost_type_id]

	# -------------------------------------------------------
	# Check if there is a where clause specified in the URL and validate the clause.
	set where_clause ""
	if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}


	# -------------------------------------------------------
	# Check if there are "valid_vars" specified in the HTTP header
	# and add these vars to the SQL clause
	set where_clause_list [list]
	foreach v $valid_vars {
		if {[info exists query_hash($v)]} {
			set value_list [split $query_hash($v) ","]
			lappend where_clause_list "$v in ([template::util::tcl_to_sql_list $value_list])" }
	}
	if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
	append where_clause [join $where_clause_list " and "]


	# Check that the query is a valid SQL where clause
	set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
	if {!$valid_sql_where} {
	im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"
	return
	}
	if {"" != $where_clause} { set where_clause "and $where_clause" }

	# Select SQL: Pull out categories.
	set sql "
		select invoice_id, invoice_nr, cost_type_id,
			customer_id, im_name_from_id(customer_id) as customer_name,
			company_contact_id, im_name_from_id(company_contact_id) as company_contact_name,
			email as company_contact_email,coalesce(home_phone,work_phone,cell_phone) as company_contact_phone,
			cost_status_id, im_name_from_id(cost_status_id) as cost_status_pretty,
			effective_date, coalesce(contact_again_date,effective_date+interval '3 days') as contact_again_date,
			amount, currency
		from im_costs c, im_invoices i left outer join parties p on (i.company_contact_id = p.party_id)
left outer join users_contact u on (u.user_id = p.party_id)
		where i.invoice_id = c.cost_id
		$where_clause
	order by invoice_nr
	"

	# Append pagination "LIMIT $limit OFFSET $start" to the sql.
	set unlimited_sql $sql
	append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

	set value ""
	set result ""
	set obj_ctr 0

	# ---------------------------------------------------------------
	# Get company notes
	# ---------------------------------------------------------------
	
	set with_notes_p 0
	if {[info exists query_hash(notes_p)] && $query_hash(notes_p) eq 1} {
		set with_notes_p 1
	}
	
	if {$with_notes_p} {
		
		# Get them from the related project and of type 11511
		set note_invoice_ids [list]
		db_foreach invoices $sql {
			lappend note_invoice_ids $invoice_id
		}
		
		# Get the related projects in an array
		set related_projects_sql "
			select distinct
				r.object_id_one as rel_project_id,
				r.object_id_two as rel_invoice_id
			from
				acs_rels r,
				im_projects p
			where
				r.object_id_one = p.project_id
				and r.object_id_two in ([template::util::tcl_to_sql_list $note_invoice_ids])
		"
		db_foreach related_project {
			if {[info exists project_ids($rel_invoice_id]} {
				lappend project_ids($rel_invoice_id) $rel_project_id
			} else {
				set project_ids($rel_invoice_id) [list $rel_project_id]
			}
		}
		
		foreach invoice_id $note_invoice_ids {

			# Get the notes from the project, but only of type 11511
			db_foreach notes "select note_id, note, note_type_id from im_notes where object_id in ([template::util::tcl_to_sql_list $project_ids($invoice_id)]) and note_type_id = 11511" {
				set note [template::util::richtext::get_property html_value $note]
				set note_teaser [string range [ad_html_text_convert -from "text/html" -to "text/plain" -- $note] 0 40]
				if {![info exists notes_js($invoice_id)]} {
				set notes_js($invoice_id) [list "\{ \"note_id\": \"$note_id\", \"note\": \"[im_quotejson $note]\", \"note_teaser\": \"[im_quotejson $note_teaser]\", \"note_type_id\": \"$note_type_id\" \}"]
				} else {
					lappend notes_js($invoice_id) "\{ \"note_id\": \"$note_id\", \"note\": \"[im_quotejson $note]\", \"note_teaser\": \"[im_quotejson $note_teaser]\", \"note_type_id\": \"$note_type_id\" \}"
				}
			}
		}
	}

	set rest_otype_read_all_p [im_permission $rest_user_id "view_invoices"]

	db_foreach objects $sql {

		# Check permissions
		set read_p $rest_otype_read_all_p
		if {!$read_p} { continue }
		
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}

		if {$with_notes_p} {
			if {[info exists notes_js($invoice_id)]} {
				append dereferenced_result ", \"notes\": \[ [join $notes_js($invoice_id) ", \n"] \]"
			} else {
				append dereferenced_result ", \"notes\": \"\""
			}
		}

		append result "$komma{\"id\": \"$invoice_id\", \"object_name\": \"[im_quotejson $invoice_nr]\"$dereferenced_result}"
		incr obj_ctr
	}

	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_sencha_project: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}
	


ad_proc -public im_rest_get_custom_sencha_qto {
	{ -action_only_p 0}
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
    Handler for POST calls on the project
} {
    
    set rest_otype "im_project"
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    }

    ns_log Notice "zebug2 $action_only_p"
    
    nsv_set quote2order_projects $project_id 1
    db_1row project_info "select p.processing_time, c.company_id, c.company_status_id, project_type_id, project_status_id, to_char(now(),'YYYY-MM-DD HH24:MI') as new_start_time 
from im_projects p, im_companies c
 where p.project_id = :project_id
and c.company_id = p.company_id"

    if {[lsearch [im_sub_categories [im_project_status_potential]] $project_status_id] < 0} {
	   nsv_unset quote2order_projects $project_id
       sencha_errors::add_error -object_id $project_id -field "Q2O" -problem "Your project $project_id is no longer in status Potential but [im_category_from_id $project_status_id]. Can't run quote2order in this case."
       sencha_errors::display_errors_and_warnings -object_id $project_id -action_type "show_modal" -modal_title "Action not possible"
    } else {
	
	if {$processing_time eq 0} {
	    im_translation_update_project_dates -project_id $project_id
	} else {
	    im_translation_update_project_dates -project_id $project_id -customer_processing_days $processing_time
	}
	
	db_dml update_project "update im_projects set project_status_id = [im_project_status_open] where project_id = :project_id"

	callback im_project_after_quote2order -project_id $project_id -type_id $project_type_id -status_id [im_project_status_open]

	if {[im_company_status_active] != $company_status_id} {
	    db_dml make_active "update im_companies set company_status_id = [im_company_status_active] where company_id = :company_id"
	}

	# Write Audit Trail - Yes, we still need that.
	cog::callback::invoke -object_type "im_project" -object_id $project_id -type_id $project_type_id -status_id [im_project_status_open] -action after_update

	nsv_unset quote2order_projects $project_id
    }

    if {!$action_only_p} {
    
	    set result "{\"success\": true, \n\"project_id\":$project_id, \n\"new_status_id\":$project_status_id }"
	    im_rest_doc_return 200 "application/json" $result
	    return
    }
}


ad_proc -public im_rest_get_custom_sencha_ptc {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
    Handler for GET call on the project which changes status of project from Potential to Canceled
} {
    
    set rest_otype "im_project"
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    }

    set result false
    set original_project_nr $project_id
    
    db_1row project_info "select p.processing_time, c.company_id, c.company_status_id, project_type_id, project_status_id, to_char(now(),'YYYY-MM-DD HH24:MI') as new_start_time 
from im_projects p, im_companies c
 where p.project_id = :project_id
and c.company_id = p.company_id"


     if {[im_project_status_open] != $project_status_id} {
       sencha_errors::add_error -object_id $project_id -field "Q2O" -problem "Your project $project_id is no longer in status [im_category_from_id [im_project_status_canceled]] but [im_category_from_id $project_status_id]"
       sencha_errors::display_errors_and_warnings -object_id $project_id -action_type "show_modal" -modal_title "Action not possible"
    } else {
	    set result true
	    db_dml update_project "update im_projects set project_status_id = [im_project_status_canceled] where project_id = :project_id"
	    callback im_project_after_potential2canceled -project_id $project_id -type_id $project_type_id -status_id [im_project_status_canceled]
        cog::callback::invoke -object_type "im_project" -object_id $project_id -type_id $project_type_id -status_id [im_project_status_canceled] -action after_update
    }
    
    set result "{\"success\": $result, \n\"project_id\":$project_id, \n\"new_status_id\":$project_status_id }"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_otd {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
    Handler for GET call on the project which changes status of project from Potential to Canceled
} {
    
    set rest_otype "im_project"
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    }

    set result false
    set original_project_nr $project_id
    
    db_1row project_info "select p.processing_time, c.company_id, c.company_status_id, project_type_id, project_status_id, to_char(now(),'YYYY-MM-DD HH24:MI') as new_start_time 
from im_projects p, im_companies c
 where p.project_id = :project_id
and c.company_id = p.company_id"

     if {[im_project_status_open] != $project_status_id} {
       sencha_errors::add_error -object_id $project_id -field "O2D" -problem "Your project $original_project_nr is no longer in status [im_category_from_id [im_project_status_open]] but [im_category_from_id $project_status_id]."
       sencha_errors::display_errors_and_warnings -object_id $project_id -action_type "show_modal" -modal_title "Action not possible"
    } else {
	    set result true
	    db_dml update_project "update im_projects set project_status_id = [im_project_status_delivered] where project_id = :project_id"
	    callback im_project_after_open2delivered -project_id $project_id -type_id $project_type_id -status_id [im_project_status_delivered]
        cog::callback::invoke -object_type "im_project" -object_id $project_id -type_id $project_type_id -status_id [im_project_status_delivered] -action after_update
    }
    
    set result "{\"success\": $result, \n\"project_id\":$project_id, \n\"new_status_id\":$project_status_id }"
    im_rest_doc_return 200 "application/json" $result
    return
}

ad_proc -public im_rest_get_custom_sencha_dynview_data {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
    Handler for GET calls on the project
} {

    set rest_otype "im_view"
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(view_id)]} {
        set view_id $query_hash(view_id)
    }


    set user_id $rest_user_id

    set obj_ctr_columns 0
    set obj_ctr_rows 0
    set komma ",\n"
    set res ""
    set column_vars ""
    set json_columns ""
    #im_view_set_def_vars -view_id $view_id -array_name view_vars
    set column_sql "select column_name, column_render_tcl, variable_name, visible_for
                    from im_view_columns
                    where view_id=:view_id and group_id is null
                    order by sort_order"

    
    db_foreach column_list_sql $column_sql {
    	set komma ",\n"
    	if {0 == $obj_ctr_columns} { set komma "" }
    	append json_columns "$komma{\"name\": \"[im_quotejson "$column_name"]\", \"variable\": \"[im_quotejson "$column_render_tcl"]\" }"
    	lappend column_vars "$variable_name"
    	incr obj_ctr_columns
    }


    set json_rows ""

    set cur_format [im_l10n_sql_currency_format]
    set date_format [im_l10n_sql_date_format]
    set today [lindex [split [ns_localsqltimestamp] " "] 0]

    set view_sql [db_string view_sql "select view_sql from im_views where view_id=:view_id"]
    
    set obj_ctr 0
    db_foreach row $view_sql {
    	set komma_rows ",\n"
    	if {0 == $obj_ctr_rows} { set komma_rows "" }
    	append json_rows "$komma_rows\{"
        db_foreach column $column_sql {
        	if {$variable_name ne ""} {
                set komma ",\n"
                eval "set v $$variable_name"
                if {0 == $obj_ctr} { set komma "" }
                append json_rows "$komma\"$variable_name\": \"[im_quotejson $v]\""
                incr obj_ctr
            }
        }
        set obj_ctr 0
        append json_rows "\}"
        incr obj_ctr_rows
    }

    set all_data ""
    append all_data \{\"view_id\": $view_id\}
    
	set result "{\"success\": true, \"total_columns\": $obj_ctr_columns, \n\"columns\": \[\n$json_columns\n\], \"total_rows\": $obj_ctr_rows,\n\n\"rows\": \[\n$json_rows\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return

}



ad_proc -public im_rest_get_custom_sencha_dynfields_defaults {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
    GET endpoint which returns dynfields default attributes
    Endpoint accepts @param attribute_ids (comma seperated ids) or @param attribute_names
} {

    set error ""
    set attribute_json_result ""

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(attribute_ids)]} {
    	set attribute_ids_exist true
        set attribute_ids $query_hash(attribute_ids)
    } else {
    	set attribute_ids_exist false
    }

    if {[info exists query_hash(attribute_names)]} {
    	set attribute_names_exist true
        set attribute_names $query_hash(attribute_names)
    } else {
    	set attribute_names_exist false
    }

    if {!$attribute_ids_exist && !$attribute_names_exist} {
    	set success false
    	set error "You must provide at last one attribuet_id or at last one attribute_name"
    } else {
    	set success true
    }


    if {$success ne false} {
        set obj_ctr 0

        set formatted_ids ""

        if {$attribute_ids_exist} {
            foreach field_id [split $attribute_ids ","] {
                set formatted_ids [concat $formatted_ids $field_id]
            }
        }

        if {$attribute_names_exist} {
            set formatted_names ""
            foreach field_name [split $attribute_names ","] {
                set formatted_names [concat $formatted_names $field_name]
            }
            set sql "select da.attribute_id from acs_attributes a, im_dynfield_attributes da where da.acs_attribute_id = a.attribute_id and a.attribute_name in ([template::util::tcl_to_sql_list $formatted_names]) and object_type = 'im_project'"
    
            db_foreach attribute_id_from_name $sql {
            	set formatted_ids [concat $formatted_ids $attribute_id]
            }
        }

        set where_clause "attribute_id in ([template::util::tcl_to_sql_list $formatted_ids])"
              
        set default_value_sql "select distinct attribute_id, default_value from im_dynfield_type_attribute_map where $where_clause"
        
        db_foreach field $default_value_sql {

    	    set komma ",\n"
    	    if {0 == $obj_ctr} { set komma "" }
            set check_first_three_letters [string range $default_value 0 2] 
            if {$check_first_three_letters eq "tcl"} {
                if {[lindex $default_value] > 0} {
                    set value [[lindex $default_value 1]]
                }
            } else {
                set value $default_value
            }
            set att_id $attribute_id
            set attribute_name [db_string attr_name "select attribute_name from acs_attributes a, im_dynfield_attributes da where da.acs_attribute_id = a.attribute_id and da.attribute_id =:att_id"]
            append attribute_json_result "$komma{\"object_id\": $attribute_id, \"attribute_id\":$attribute_id, \"attribute_value\":\"$value\", \"attribute_name\":\"$attribute_name\"}"
    	    incr obj_ctr
    
        }
    }

	set result "{\"success\": $success, \"error\": \"$error\",\n\"data\": \[\n$attribute_json_result\n\]}"
	im_rest_doc_return 200 "application/json" $result
	return

}


ad_proc -public im_rest_get_custom_sencha_cost_centers {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for GET rest call to get cost centers
} {

    set obj_ctr 0
    set komma ",\n"
    set data ""



   set cost_centers_sql "select cost_center_id, cost_center_name, cost_center_code from im_cost_centers"

   db_foreach cost_center $cost_centers_sql {
       set komma ",\n"
       if {0 == $obj_ctr} { set komma "" }
       append data "$komma{\"cost_center_id\":$cost_center_id, \"cost_center_name\":\"$cost_center_name\", \"cost_center_code\":\"$cost_center_code\"}"
       incr obj_ctr
   }



	set result "{\"success\": 1, \"error\": \"0\",\n\"data\": \[\n$data\n\]}"
	im_rest_doc_return 200 "application/json" $result
	return

}

ad_proc -public im_rest_get_custom_get_freelancers {
    { -project_id ""}
    { -company_status_ids ""}
    { -language_experience_level_id ""}
    { -source_language_ids ""}
    { -target_language_ids ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for getting information on Translation freelancers
    
    @param project_id ID of the project for which we are looking for freelancers.
    @param company_status_ids List of company status ids which allow us to limit the query for freelance companies. Typically set to open and potential
    @param language_experience_level_id Used to differentiate between certified translators and others
    @param source_language_ids Comma separated List of source languages to look for
    @param target_language_ids comma separated list of target languages to look for

    @return freelancer_id UserID of the freelancer
    @return first_names First Names of the freelancer
    @return last_name Last Name
    @return source_language_ids List of source languages the freelancer knows
    @return source_language_ids List of target languages the freelancer knows
    @return skill_2002
    @return skill_2014
    @return skill_2000
    @return skill_2024
    @return skill_2006
    @return skill_10000432
    @return company_status_id Status of the company
    @return price String with the price for the translation per source word. Normal translation, unless there is only a price for certified translations. 
    @return country Country Code for the freelancer
    @return country_code AGAIN... country code for the freelancer
    @return country_name Name of the country
    @return skill_ids List of all skill_ids of the freelancer not mentioned above
    @return skills JSON for the skill ids above
    @return subject_area_skill_names Names of the subject area skills
    @return skill_names Names of all the other skills 
    @return quality Calculated quality of the freelancer (to be described how this is done)
    @return note_exists_p Do we have a note for the freelancer

} {


    array set query_hash $query_hash_pairs
   
    # We need both source_language and target_language skill_type_id
    set source_language_skill_type_id [im_freelance_skill_type_source_language]
    set target_language_skill_type_id [im_freelance_skill_type_target_language]
    set subject_area_skill_type_id [im_freelance_skill_type_subject_area]


    # as default we do not want to fetch freelancer prices
    set include_prices_p 0
    

    # Company status filter
    if {$company_status_ids eq ""} {
        # if we send empty company status_ids we are still filtering it
        set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]
    }

    if {$language_experience_level_id ne ""} {
        set language_experience_level_sql "and confirmed_experience_id >=:language_experience_level_id"
    } else {
        set language_experience_level_sql ""
    }
    

    set other_sql ""

    set source_languages_sql ""
    set count 0
    if {[llength $source_language_ids] > 0} {
    	
    	append source_languages_sql "AND ("
    	foreach source_language_id $source_language_ids {
    		if {$count == 0} {
                append source_languages_sql " (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:source_language_skill_type_id and skill_id =$source_language_id $language_experience_level_sql) > 0"
            } else {
                append source_languages_sql " OR (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:source_language_skill_type_id and skill_id =$source_language_id $language_experience_level_sql) > 0"
            }
            incr count
        }
        append source_languages_sql ")"

    } else {
    	# we also might want to have search for freelancer with requried level on ANY langguage
        if {$language_experience_level_id ne ""} {
            append other_sql " AND (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:source_language_skill_type_id $language_experience_level_sql) > 0"
        }
    }

    set target_languages_sql ""
    set count 0
    if {[llength $target_language_ids] > 0} {
    	
    	append target_languages_sql "AND ("
    	foreach target_language_id $target_language_ids {
    		if {$count == 0} {
                append target_languages_sql " (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:target_language_skill_type_id and skill_id =$target_language_id $language_experience_level_sql) > 0"
            } else {
                append target_languages_sql " OR (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:target_language_skill_type_id and skill_id =$target_language_id $language_experience_level_sql) > 0"
            }
            incr count
        }
        append target_languages_sql ")"

    } else {
    	# we also might want to have search for freelancer with requried level on ANY langguage
        if {$language_experience_level_id ne ""} {
            append other_sql " AND (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:target_language_skill_type_id $language_experience_level_sql) > 0"
        }
    }


    # We check if both Target Language and Source Language filters are empty. 
    # If yes, then we still exclude Freelancers who do not know any language, so data won't be too big
	if {$target_languages_sql eq "" && $source_languages_sql eq ""} {
	    append other_sql " AND (select count (*) from im_freelance_skills where user_id = m.member_id and skill_type_id=:target_language_skill_type_id $language_experience_level_sql) > 0"
	}

    # we might want to include prices
    # set default_task_type_id 4210
    # set second_default_task_type_id 4218
    # task type id in reality is project_type_id
    set default_task_type_id 93
    set second_default_task_type_id 2505
    set default_unit_of_measure_id [im_uom_s_word]
    # if both source language and SINGLE target language were provided, we want to fetch for prices
    if {[llength $source_language_ids] > 0 && [llength $target_language_ids] > 0} {
    	set include_prices_p 1
    	set source_language_for_price_id $source_language_id
    	set target_language_for_price_id $target_language_id
    } 

	set valid_vars [list freelancer_name price_s_word price_hour selected_p potential_p active_project_member_p matched_target_lang_id source_language_names target_language_names sort_order source_language_exp_level target_language_exp_level matched_lang_exp_level existing_assignment_status_id]

    set obj_ctr 0
    set komma ",\n"
    set data ""
   
	set freelance_sql_original "
		select distinct
			m.member_id as freelancer_id
		from
			group_member_map m,
			(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
		where m.group_id = [im_profile_freelancers] 
		and m.member_id not in (
				-- Exclude banned or deleted users
				select	m.member_id
				from	group_member_map m,
					membership_rels mr
				where	m.rel_id = mr.rel_id and
					m.group_id = acs__magic_object_id('registered_users') and
					m.container_id = m.group_id and
					mr.member_state != 'approved') 
			AND
			f.object_id_two = m.member_id	
			$source_languages_sql
			$target_languages_sql	
			$other_sql
			order by m.member_id desc
		"
   
   set country_codes [im_country_options]
   set max_quality_projects 0

   db_foreach freelancer $freelance_sql_original {

       set komma ",\n"
       if {0 == $obj_ctr} { set komma "" }
       set user_data [db_1row user_data "select * from cc_users where user_id=:freelancer_id"]
       set full_name [im_quotejson "$first_names $last_name"]
       set source_language_ids ""
       set target_language_ids ""
       set source_language_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:source_language_skill_type_id" ]
       set target_language_ids [db_list target_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:target_language_skill_type_id" ]
       
       
       set source_language_names [list]
       set target_language_names [list]
       set all_other_freelancer_skills_ids [db_list all_other_freelancer_skills_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id<>:source_language_skill_type_id and skill_type_id<>:target_language_skill_type_id" ]
       
       set all_skills_with_levels "select skill_id, skill_type_id, confirmed_experience_id, aux_html1 as level_style_css from im_freelance_skills inner join im_categories on (category_id = confirmed_experience_id) where user_id =:freelancer_id and (skill_type_id =:subject_area_skill_type_id or skill_type_id=:source_language_skill_type_id or skill_type_id=:target_language_skill_type_id)"

       set all_other_freelancer_skills_ids_sql "select skill_id, skill_type_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id<>:source_language_skill_type_id and skill_type_id<>:target_language_skill_type_id"
       set all_other_freelancer_skills_names [list]
       set subject_area_skills_names [list]
       set all_other_freelancer_skills_json ""

       set all_skills_komma ",\n"
       set all_skills_counter 0

       set all_skill_types [list]
       set skill_2014_list [list]
       set skill_2024_list [list]
       set skill_2006_list [list]
       set skill_2028_list [list]
       set skill_10000432_list [list]


       db_foreach skill $all_skills_with_levels {
       	   set all_skills_komma ",\n"
           if {0 == $all_skills_counter} { set all_skills_komma "" }
       	   set skill_name [im_category_from_id $skill_id]

           lappend all_other_freelancer_skills_names $skill_name
           set skill_type_name [im_category_from_id $skill_type_id]
           append all_other_freelancer_skills_json "$all_skills_komma{\"id\":$skill_id, \"skill_id\":$skill_id, \"skill_name\": \"$skill_name\", \"skill_type_id\":$skill_type_id, \"skill_type_name\": \"$skill_type_name\", \"confirmed_experience_id\":\"$confirmed_experience_id\", \"level_style_css\":\"$level_style_css\"}"
           incr all_skills_counter
           lappend all_skill_types \"$skill_type_name\"
           if {$skill_type_id eq 2014} {
               lappend skill_2014_list $skill_id
               lappend subject_area_skills_names $skill_name
           }
           if {$skill_type_id eq 2024} {
               lappend skill_2024_list $skill_id
           }
           if {$skill_type_id eq 2006} {
               lappend skill_2006_list $skill_id
           }
           if {$skill_type_id eq 10000432} {
               lappend skill_10000432_list $skill_id
           }
       }
       set all_skill_types [join $all_skill_types ", "]
       set source_language_ids [join $source_language_ids ","]
       set target_language_ids [join $target_language_ids ","]
       set source_language_names [join $source_language_names ", "]
       set target_language_names [join $target_language_names ", "]
       set skill_2014_list [join $skill_2014_list ","]
       set skill_2024_list [join $skill_2024_list ","]
       set skill_2006_list [join $skill_2006_list ","]
       set skill_2028_list [join $skill_2028_list ","]
       set skill_10000432_list [join $skill_10000432_list ","]


       set all_other_freelancer_skills_names [join $all_other_freelancer_skills_names ", "]
       set subject_area_skills_names [join $subject_area_skills_names ", "]
       set skill_2002 "\[$target_language_ids\]"
       set skill_2000 "\[$source_language_ids\]"
       set skill_2014 "\[$skill_2014_list\]"
       set skill_2024 "\[$skill_2024_list\]"
       set skill_2006 "\[$skill_2006_list\]"
       set skill_10000432 "\[$skill_10000432_list\]"

       set freelancer_company_id [im_translation_freelance_company -freelance_id $freelancer_id]
       set company_data [db_0or1row freelancer_data "select * from im_companies where company_id =:freelancer_company_id"]
       set main_office_type_id [im_office_type_main]
       set address_country_code ""
       set main_office_data [db_0or1row main_office_data "select address_country_code from im_offices where company_id=:freelancer_company_id and office_type_id=:main_office_type_id"]
       set freelancer_country_name ""
       foreach country $country_codes {
           set country_code_ch [lindex $country 1]
           set country_name_ch [lindex $country 0]
           if {$address_country_code eq $country_code_ch} {
              set freelancer_country_name $country_name_ch
           }
       }

       # note
       set note_exists_p [db_string note_exist "select 1 from im_notes where object_id = :freelancer_id limit 1" -default 0]

       # quality
       
       set quality_list [im_trans_freelancer_quality_json -freelancer_id $freelancer_id]
       set quality [lindex $quality_list 1]
       # Find the maximum number of projects to display in the quality bar
       set num_projects [lindex $quality_list 0]
       if {$max_quality_projects < $num_projects} {
           set max_quality_projects $num_projects
       }

       set quality_list [util_memoize "im_trans_freelancer_quality_json -freelancer_id $user_id" 3600]
       set quality [lindex $quality_list 1]
   

	   # prices - default values
	   set prices ""
	   set obj_ctr_prices 0
	   set komma_prices ""
	   set price 0
	   set min_price 0
	   set currency ""
	   set task_type_sql "and task_type_id=:default_task_type_id"
	   if {$include_prices_p == 1} {
           
           # if language experience level is provided, we need to change default_task_type_id
           if {$language_experience_level_id ne ""} {
               set possible_default_task_type_id [db_string possible_default_task_type_id "select aux_int2 from im_categories where category_id =:language_experience_level_id" -default ""]
               if {$possible_default_task_type_id ne ""} {
                   set default_task_type_id $possible_default_task_type_id
               }
           }
       
           set prices_sql "select * from im_trans_prices where company_id =:freelancer_company_id and uom_id =:default_unit_of_measure_id and target_language_id in ([template::util::tcl_to_sql_list $target_language_for_price_id]) and source_language_id =:source_language_for_price_id $task_type_sql order by price asc limit 1"
           set price_exist_p [db_0or1row single_price $prices_sql]
           # If no language exprience level was provided and we didn't get any price for 4210 (trans), we still loook for 4218 (cert)
           if {$price_exist_p eq 0} {
           	 set task_type_sql "and task_type_id=$second_default_task_type_id"
           	 set prices_sql "select * from im_trans_prices where company_id =$freelancer_company_id and uom_id =$default_unit_of_measure_id and target_language_id in ([template::util::tcl_to_sql_list $target_language_for_price_id]) and source_language_id =$source_language_for_price_id $task_type_sql order by price asc limit 1"
           	 db_0or1row single_price $prices_sql
           }
           set price_formatted [expr {double(round(100*$price))/100}]
           if {$min_price > 0} {
           	   set formatted_min_price [format "%.2f" [expr {double(round(100*$min_price))/100}]]
               set price_display "$price_formatted $currency ($formatted_min_price $currency)"
           } else {
               set price_display "$price_formatted $currency"
           }
           set price $price_display
	   }
       
       append data "$komma{\"id\":$user_id,\"object_id\":$user_id, \"freelancer_id\":$user_id, \"first_names\":\"[im_quotejson $first_names]\", \"last_name\":\"[im_quotejson $last_name]\", \"freelancer_name\":\"$full_name\", 
           \"source_language_ids\":\"$source_language_ids\", \"target_language_ids\":\"$target_language_ids\", \"skill_2002\":\"$skill_2002\",
            \"skill_2014\":\"$skill_2014\", \"skill_2000\":\"$skill_2000\",\"skill_2024\":\"$skill_2024\", \"skill_2006\":\"$skill_2006\", \"skill_10000432\":\"$skill_10000432\",
            \"company_status_id\":\"$company_status_id\", \"price\":\"$price\", \n\"max_quality_projects\": $max_quality_projects,
           \"country\":\"$address_country_code\", \"country_code\":\"$address_country_code\",\"country_name\":\"$freelancer_country_name\",
           \"skills_ids\":\"$all_other_freelancer_skills_ids\",\"subject_area_skills_names\":\"$subject_area_skills_names\", \"skills_names\":\"$all_other_freelancer_skills_names\", \"quality\":$quality,  \"note_exists_p\":$note_exists_p, \n\"skills\": \[\n$all_other_freelancer_skills_json\n\]}"
       incr obj_ctr
   }


	set result "{\"success\": 1, \"error\": \"0\", \"number_of_freelancers\":$obj_ctr, \n\"data\": \[\n$data\n\]}"
	im_rest_doc_return 200 "application/json" $result
	return
}




ad_proc -public im_rest_get_custom_get_freelancers_old_backup {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting information on Translation freelancers

	Requires the project_id

	Returns a JSON with
	- Freelancer ID and name
	- Source & Target languages
	- Skills (to be defined which ones)
	- Rates (source word & hours)

	See /sencha-freelance-translation/lib/freelance-trans-member.tcl for details
} {
   
   set project_id 0
	
	# Anyone who can write on this project can query the freelancers for it
	im_project_permissions $rest_user_id $project_id view_p read_p write_p admin_p
	if {!$write_p} {return}

	set valid_vars [list freelancer_name price_s_word price_hour selected_p potential_p active_project_member_p matched_target_lang_id source_language_names target_language_names sort_order source_language_exp_level target_language_exp_level matched_lang_exp_level existing_assignment_status_id]

	# Get the skill information for the skills to return
	set skill_type_sql ""

	db_foreach skill_types {select skill_type_id, object_type_id, display_mode
		from im_freelance_skill_type_map, im_projects
		where object_type_id = project_type_id and project_id = :project_id and display_mode != 'none'
	} {
			append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = m.member_id) as skill_$skill_type_id,\n"
#			append skill_type_sql "\t\tim_freelance_skill_list(m.member_id, $skill_type_id) as skill_$skill_type_id,\n"
			lappend valid_vars "skill_$skill_type_id"
	}
	# Source & Target
	foreach skill_type_id [list 2000 2002] {
		if {[lsearch $valid_vars skill_$skill_type_id]<0} {
				append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = m.member_id) as skill_$skill_type_id,\n"
				lappend valid_vars "skill_$skill_type_id"
		}
	}

	# ---------------------------------------------------------------
	# Languages
	# ---------------------------------------------------------------

	if {$project_id eq 0} {


		set target_language_ids [list]
		set source_language_ids [list]
        set sql "select l.language_id as target_language
        from im_target_languages l
	    "
	    db_foreach select_target_languages $sql {
	       lappend target_language_ids $target_language
	       lappend source_language_ids $target_language
        }
	}

	# Override if we have the skill_2002
	if {[info exists query_hash(skill_2000)]} {
		set source_language_ids $query_hash(skill_2000)
	}
	if {$source_language_ids ne ""} {
		set source_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2000) source_lang"
		set source_lang_where " AND m.member_id = source_lang.user_id"
	} else {
		set source_lang_from ""
		set source_lang_where ""
		set source_lang ""
	}


	# Override if we have the skill_2002
	if {[info exists query_hash(skill_2002)]} {
		set target_language_ids $query_hash(skill_2002)
	}
	if {$target_language_ids ne ""} {
		set target_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2002) target_lang"
		set target_lang_where " AND m.member_id = target_lang.user_id"
	} else {
		set target_lang_from ""
		set target_lang_where ""
		set target_lang ""
	}

	set customer_id [db_string company "select coalesce(final_company_id,company_id) as company_id from im_projects where project_id = :project_id" -default "0"]

	set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]

	# Get the project_type to find out the order of trans steps
	set trans_steps [split [db_string get_steps "select aux_string1 from im_categories c, im_projects p where p.project_id = :project_id and p.project_type_id = c.category_id" -default ""] " "]
	
	set freelance_sql "
		select distinct
			m.member_id as user_id
		from
			group_member_map m,
			(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
			$source_lang_from
			$target_lang_from
		where m.group_id = [im_profile_freelancers] and m.member_id not in (
				-- Exclude banned or deleted users
				select	m.member_id
				from	group_member_map m,
					membership_rels mr
				where	m.rel_id = mr.rel_id and
					m.group_id = acs__magic_object_id('registered_users') and
					m.container_id = m.group_id and
					mr.member_state != 'approved') AND
			f.object_id_two = m.member_id
			$source_lang_where
			$target_lang_where
		"
			
	# ---------------------------------------------------------------
	# Build the price array for freelancers
	# ---------------------------------------------------------------
   	set price_sql "
		select
			f.user_id,
			p.uom_id,
			im_category_from_id(p.task_type_id) as task_type,
			im_category_from_id(p.source_language_id) as source_language,
			im_category_from_id(p.target_language_id) as target_language,
			im_category_from_id(p.subject_area_id) as subject_area,
			im_category_from_id(p.file_type_id) as file_type,
			min(p.price) as min_price,
			max(p.price) as max_price
		from
			($freelance_sql) f
			LEFT OUTER JOIN acs_rels uc_rel ON (f.user_id = uc_rel.object_id_two)
			LEFT OUTER JOIN im_trans_prices p ON (uc_rel.object_id_one = p.company_id)
		where
			(p.source_language_id in ([template::util::tcl_to_sql_list $source_language_ids]) or
				p.source_language_id is null)
			and (p.target_language_id in ([template::util::tcl_to_sql_list $target_language_ids]) or
				p.target_language_id is null)
		group by
			f.user_id,
			p.uom_id,
			p.task_type_id,
			p.source_language_id,
			p.target_language_id,
			p.subject_area_id,
			p.file_type_id
			order by task_type"

	db_foreach price_hash $price_sql {
		set key "$user_id-$uom_id"

		# Calculate the base cell value
		set price_append "$min_price - $max_price"
		if {$min_price == $max_price} { set price_append "$min_price" }
		# Add the list of parameters
		set param_list [list $price_append $source_language $target_language]
		if {"" == $source_language && "" == $target_language} { set param_list [list $price_append] }
		if {"" != $subject_area} { lappend param_list $subject_area }
		if {"" != $task_type} { lappend param_list $task_type }
		if {"" != $file_type} { lappend param_list $file_type }

		# Update the hash table cell
		set hash ""
		if {[info exists price_hash($key)]} { set hash $price_hash($key) }
		set hash [im_freelance_trans_member_select_component_join_prices $hash $param_list]
		set price_hash($key) $hash
	}

	foreach key [array names price_hash] {
		set values $price_hash($key)
		set result_list [list]
		foreach value $values {
			lappend result_list "\"[lindex $value 0] {[lrange $value 1 end]}\""
		}
		set price_hash($key) "\[[join $result_list ,]\]"
	}

	# ---------------------------------------------------------------
	# Generate the JSON for each freelancer
	# ---------------------------------------------------------------

	set obj_ctr 0
	
    # ---------------------------------------------------------------
    # Generate the from clauses for sorting
    # ---------------------------------------------------------------
    set from_sql ""

    # Get the confirmed assignees
    set confirmed_assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments fa, im_freelance_packages fp 
         where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id and assignment_status_id in (4222,4225,5225,4226,4227,4229)"]

    if {$confirmed_assignee_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $confirmed_assignee_ids]) then '1' else '0' end as confirmed_assignee_p,"
    } else {
	append from_sql "0 as confirmed_assignee_p,"
    }


    # Get the requested assignees
    set requested_assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments fa, im_freelance_packages fp 
         where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id and assignment_status_id in (4221,4220)"]

    if {$requested_assignee_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $requested_assignee_ids]) then '1' else '0' end as requested_assignee_p,"
    } else {
	append from_sql "0 as requested_assignee_p,"
    }
    

    # Get the project members
    set project_member_ids [im_biz_object_member_ids $project_id]


    # Assignees are also project members
    set project_member_ids [concat $project_member_ids $confirmed_assignee_ids $requested_assignee_ids]	

    if {$project_member_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $project_member_ids]) then '1' else '0' end as active_project_member_p,"
    } else {
	append from_sql "0 as active_project_member_p,"
    }
    
    # Get the main translators
    set sql "select distinct freelancer_id from im_trans_main_freelancer where customer_id = :customer_id and source_language_id in ([template::util::tcl_to_sql_list $source_language_ids]) and target_language_id in ([template::util::tcl_to_sql_list $target_language_ids])"
    set main_translator_ids [db_list freelancer "$sql"]

    if {$main_translator_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $main_translator_ids]) then '1' else '0' end as main_trans_p,"
    } else {
	append from_sql "0 as main_trans_p,"
    }

    # Potential Members should be shown afterwards
    set potential_status_ids [im_sub_categories [im_company_status_potential]]


    set source_language_exp_level_sql "(select max(im_categories.aux_int1) AS experience from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = m.member_id AND im_freelance_skills.skill_id in ([template::util::tcl_to_sql_list $source_language_ids]) AND im_freelance_skills.skill_type_id = 2000) as source_lang_experience,"
	

	#set add_language_exp_level_sql "(select im_categories.aux_int1 AS experience from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = m.member_id AND im_freelance_skills.skill_id in ([template::util::tcl_to_sql_list $target_language_ids]) AND im_freelance_skills.skill_type_id = 2002) as target_experien,"

	set sort_order 0
	set max_quality_projects 0

    set freelancer_ids 	[db_list freelancers "select distinct
				m.member_id as user_id
			from
				group_member_map m,
				(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
				$source_lang_from
				$target_lang_from
			where m.group_id in ([im_profile_freelancers],[im_profile_employees]) and m.member_id not in (
					-- Exclude banned or deleted users
					select	m.member_id
					from	group_member_map m,
						membership_rels mr
					where	m.rel_id = mr.rel_id and
						m.group_id = acs__magic_object_id('registered_users') and
						m.container_id = m.group_id and
						mr.member_state != 'approved') AND
				f.object_id_two = m.member_id
				$source_lang_where
				$target_lang_where"]

			 ns_log Notice "freelancers... $freelancer_ids"

	set freelance_sql "
			select distinct
				m.member_id as user_id,
				$source_language_exp_level_sql
				im_name_from_user_id(m.member_id) as object_name,
				$skill_type_sql
				$from_sql
				case when company_status_id in ([template::util::tcl_to_sql_list $potential_status_ids]) then '1' else '0' end as potential_p
			from
				group_member_map m,
				(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
				$source_lang_from
				$target_lang_from
			where m.group_id in ([im_profile_freelancers],[im_profile_employees]) and m.member_id not in (
					-- Exclude banned or deleted users
					select	m.member_id
					from	group_member_map m,
						membership_rels mr
					where	m.rel_id = mr.rel_id and
						m.group_id = acs__magic_object_id('registered_users') and
						m.container_id = m.group_id and
						mr.member_state != 'approved') AND
				f.object_id_two = m.member_id
				$source_lang_where
				$target_lang_where
			order by object_name desc
		" 
    ns_log Notice "$freelance_sql"
    db_foreach select_freelancers $freelance_sql {
		incr sort_order
		set freelancer_name $object_name
		set source_language_exp_level $source_lang_experience

		# Append the prices
		set uom_listlist "{324 price_s_word} {320 price_hour}"
		foreach uom_tuple $uom_listlist {
			set uom_id [lindex $uom_tuple 0]
			set var_name [lindex $uom_tuple 1]
			set key "${user_id}-${uom_id}"
			set $var_name ""
			if { [info exists price_hash($key)] } {
				set $var_name $price_hash($key)
			}
		}


		# ---------------------------------------------------------------
		# Matched Target Language
		# ---------------------------------------------------------------
		set matched_target_lang_id [list]
		set target_language_names [list]
		set source_language_names [list]
		
		regsub -all {\[} $skill_2002 {} target_skill_ids
		regsub -all {\]} $target_skill_ids {} target_skill_ids
		foreach match_target_lang_id [split $target_skill_ids ","] {
			if {[lsearch $target_language_names [im_name_from_id $match_target_lang_id]] == -1} {
				lappend target_language_names [im_name_from_id $match_target_lang_id]
			}
		}

		set target_language_names [join $target_language_names ","]


		regsub -all {\[} $skill_2000 {} source_skill_ids
		regsub -all {\]} $source_skill_ids {} source_skill_ids
		foreach match_source_lang_id [split $source_skill_ids ","] {
			if {[lsearch $source_language_names [im_name_from_id $match_source_lang_id]] == -1} {
				lappend source_language_names [im_name_from_id $match_source_lang_id]
			}
		}

		set source_language_names [join $source_language_names ","]

        db_0or1row target_language_exp_level_sql "select im_categories.aux_int1 AS target_language_exp_level from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = :user_id AND im_freelance_skills.skill_id in ([template::util::tcl_to_sql_list $match_target_lang_id]) AND im_freelance_skills.skill_type_id = 2002"
		set $target_language_exp_level target_language_exp_level

		db_0or1row matched_lang_experience_level "select im_categories.aux_int1 AS target_lang_experience_from_sql_int, im_categories.category AS target_lang_experience_from_sql_string  from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = :user_id AND im_freelance_skills.skill_id in([template::util::tcl_to_sql_list $match_target_lang_id]) AND im_freelance_skills.skill_type_id = 2002"
        set matched_lang_exp_level $target_lang_experience_from_sql_string
		# ---------------------------------------------------------------
		# Active Project Members have the relationship_id set
		# ---------------------------------------------------------------
		set object_id $user_id
		if {[lsearch $project_member_ids $user_id]>-1} {
			set selected_p 1
		} else {
		    set selected_p 0
		}
		
		# ---------------------------------------------------------------
		# Quality JSON
		# ---------------------------------------------------------------
		set quality_list [im_trans_freelancer_quality_json -freelancer_id $user_id]
		set quality [lindex $quality_list 1]
		
		# Find the maximum number of projects to display in the quality bar
		set num_projects [lindex $quality_list 0]
		if {$max_quality_projects < $num_projects} {
			set max_quality_projects $num_projects
		}


        set existing_assignment_status_id [db_string check_existing_assignment_status "select assignment_status_id from im_freelance_assignments fa, im_freelance_packages fp where assignee_id =:user_id and project_id =:project_id and fa.freelance_package_id = fp.freelance_package_id order by assignment_id desc limit 1" -default 0]
	    
		switch $format {
			html {
			append result "<tr>
				<td>$rest_oid</td>
				<td><a href=\"$url?format=html\">$project_name</a></td>
				<td>$project_manager</td>
			</tr>\n"
			}
		json {
			set komma ",\n"
			if {0 == $obj_ctr} { set komma "" }
			set dereferenced_result ""
			foreach v $valid_vars {
				eval "set a $$v"
				regsub -all {\n} $a {\n} a
				regsub -all {\r} $a {} a
				append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
			}
			
			append dereferenced_result ", \"quality\": $quality"

			append result "$komma{\"id\": \"$object_id\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}"
		}
		default {}
	}
		incr obj_ctr
	}

	set project_status_id [db_string project_status_id "select project_status_id from im_projects where project_id=:project_id" -default 0]

	switch $format {
		html {
			set page_title "object_type: $rest_otype"
			im_rest_doc_return 200 "text/html" "
			[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
			<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
			</table>[im_footer]
			"
		}
		json {
			set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"project_status_id\": $project_status_id,\n\"message\": \"im_rest_get_custom_trans_freelancers: Data loaded..\",\n\"max_quality_projects\": $max_quality_projects,\n\"data\": \[\n$result\n\]\n}"
			im_rest_doc_return 200 "application/json" $result
			return
		}
	}
	return
}



ad_proc -public im_rest_get_custom_sencha_single_freelancer {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Endpoint returning single freelancer data, fetched with @param freelancer_id (which truy is @param user_id)
} {


    array set query_hash $query_hash_pairs

    if {[info exists query_hash(freelancer_id)]} {
        set freelancer_id $query_hash(freelancer_id)
        set result true
        set error ""
    } else {
    	set result false
    	set error "You must provide freelancer_id"
    	set freelancer_id 0
    }    
    
    set default_note_type_id [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "DefaultNoteTypeId" -default "11514"]
    
    if {$freelancer_id ne 0} {
    	set error "No such freelancer"
    	set freelancer_data [db_0or1row freelancer_data "select * from cc_users where user_id = :freelancer_id"]
        if {$freelancer_data ne 0} {
            set freelancer_company_id [im_translation_freelance_company -freelance_id $freelancer_id]
    	    set company_data [db_0or1row freelancer_data "select * from im_companies where company_id =:freelancer_company_id"]
            set company_office_id $main_office_id
            set main_office_data  [db_0or1row main_office_data "select * from im_offices where office_id =:company_office_id"]
            # note
            set note_to_display ""
            set note ""
            set note_exists_p 0

            if {$default_note_type_id ne ""} {
                set note [db_string get_one_note "select note from im_notes where object_id =:freelancer_id and note_type_id =:default_note_type_id order by note_id desc limit 1" -default ""]
                if {$note eq ""} {
                	set note [db_string get_one_note "select note from im_notes where object_id =:freelancer_id order by note_id desc limit 1" -default ""]
                } 
                if {$note ne ""} {
            	    set note_exists_p 1
            	    set note_to_display [lindex $note 0]
                }
            }
            # languages
            set source_language_ids ""
            set target_language_ids ""
            set source_language_skill_type_id [im_freelance_skill_type_source_language]
            set target_language_skill_type_id [im_freelance_skill_type_target_language]
            set subject_area_skill_type_id [im_freelance_skill_type_subject_area]
	        set certified_experience_id 2204
            #set source_language_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:source_language_skill_type_id and confirmed_experience_id <>:certified_experience_id" ]
            set source_language_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:source_language_skill_type_id"]
            set target_language_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:target_language_skill_type_id"]
            set cert_source_language_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:source_language_skill_type_id and confirmed_experience_id = :certified_experience_id" ]
            set cert_target_language_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:target_language_skill_type_id and confirmed_experience_id = :certified_experience_id" ]
            set subject_area_ids [db_list source_language_ids "select skill_id from im_freelance_skills where user_id =:freelancer_id and skill_type_id=:subject_area_skill_type_id" ]
            set source_language_ids [join $source_language_ids ","]
            set target_language_ids [join $target_language_ids ","]
            set cert_source_language_ids [join $cert_source_language_ids ","]
            set cert_target_language_ids [join $cert_target_language_ids ","]
            set subject_area_ids [join $subject_area_ids ","]
            set error ""
            set freelancer_all_data ""
            set komma ""
            append freelancer_all_data "$komma{\"field_name\":\"object_id\", \"field_value\":$freelancer_id}"
            set komma ","
            append freelancer_all_data "$komma{\"field_name\":\"freelancer_id\", \"field_value\":$user_id}"
            append freelancer_all_data "$komma{\"field_name\":\"company_id\", \"field_value\":$freelancer_company_id}"
            append freelancer_all_data "$komma{\"field_name\":\"first_names\", \"field_value\":\"$first_names\"}"
            append freelancer_all_data "$komma{\"field_name\":\"last_name\", \"field_value\":\"$last_name\"}"

            append freelancer_all_data "$komma{\"field_name\":\"email\", \"field_value\":\"$email\"}"
            append freelancer_all_data "$komma{\"field_name\":\"company_name\", \"field_value\":\"$company_name\"}"
            append freelancer_all_data "$komma{\"field_name\":\"company_status_id\", \"field_value\":$company_status_id}"
            append freelancer_all_data "$komma{\"field_name\":\"company_type_id\", \"field_value\":$company_type_id}"
            append freelancer_all_data "$komma{\"field_name\":\"main_office_id\", \"field_value\":$company_office_id}"

            append freelancer_all_data "$komma{\"field_name\":\"address\", \"field_value\":\"$address_line1\"}"
            append freelancer_all_data "$komma{\"field_name\":\"telephone\", \"field_value\":\"$phone\"}"
            append freelancer_all_data "$komma{\"field_name\":\"city\", \"field_value\":\"$address_city\"}"
            append freelancer_all_data "$komma{\"field_name\":\"country\", \"field_value\":\"$address_country_code\"}"


            append freelancer_all_data "$komma{\"field_name\":\"zipcode\", \"field_value\":\"$address_postal_code\"}"
            append freelancer_all_data "$komma{\"field_name\":\"source_language_ids\", \"field_value\":\"$source_language_ids\"}"
            append freelancer_all_data "$komma{\"field_name\":\"target_language_ids\", \"field_value\":\"$target_language_ids\"}"
            append freelancer_all_data "$komma{\"field_name\":\"subject_area_ids\", \"field_value\":\"$subject_area_ids\"}"
            append freelancer_all_data "$komma{\"field_name\":\"cert_source_language_ids\", \"field_value\":\"$cert_source_language_ids\"}"
            append freelancer_all_data "$komma{\"field_name\":\"cert_target_language_ids\", \"field_value\":\"$cert_target_language_ids\"}"

            set result "{\"success\": true, \"error\":\"$error\", \"note_exists_p\":$note_exists_p, \"note\":\"[im_quotejson $note]\",\"note_to_display\":\"[im_quotejson $note_to_display]\", \n\"data\": \[\n$freelancer_all_data\n\]\n}"
        } else {
            set result "{\"success\": false, \"error\":\"$error\"}"
        }
		
    }

	im_rest_doc_return 200 "application/json" $result
	return	
	
}




ad_proc -public im_rest_get_custom_sencha_freelancer_prices {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Endpoint returning single freelancer prices
} {


    array set query_hash $query_hash_pairs

    if {[info exists query_hash(freelancer_id)]} {
        set freelancer_id $query_hash(freelancer_id)
        set result false
        set error ""
    } else {
    	set result false
    	set error "You must provide freelancer_id"
    	set freelancer_id 0
    }    
    
    if {$freelancer_id ne 0} {
    	set error "No such freelancer"
    	set freelancer_data [db_0or1row freelancer_data "select * from cc_users where user_id = :freelancer_id"]
        set freelancer_name "$first_names $last_name"
        set freelancer_company_id [im_translation_freelance_company -freelance_id $freelancer_id]
        set prices ""
        set obj_ctr 0

        set prices_sql "select * from im_trans_prices where company_id =:freelancer_company_id"
    
        db_foreach price $prices_sql  {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            append prices "$komma{\"uom_id\":\"$uom_id\", \"uom_name\":\"[im_category_from_id $uom_id]\", \"source_language_id\":\"$source_language_id\", \"source_language_name\":\"[im_category_from_id $source_language_id]\", \"target_language_id\":\"$target_language_id\", \"target_language_name\":\"[im_category_from_id $target_language_id]\",\"task_type_id\":\"$task_type_id\", \"task_type_name\":\"[im_category_from_id $task_type_id]\", \"price\":\"$price\", \"min_price\":\"$min_price\"}"
            incr obj_ctr
        }

            set result "{\"success\": true, \"freelancer_id\":$freelancer_id, \"freelancer_company_id\":$freelancer_company_id, \"freelancer_name\":\"$freelancer_name\", \n\"data\": \[\n$prices\n\]\n}"

        } else {
            set result "{\"success\": false, \"error\":\"$error\"}"
        }

	im_rest_doc_return 200 "application/json" $result
	return	
	
}

ad_proc -public im_rest_get_custom_sencha_form_dynfields {
	{ -page_url ""}
	{ -object_type ""}
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Endpoint returning i18n translations for given @package_key
    
    @param object_type Object (e.g. 'person' or 'company') for which we want to get dynfields
    @param page_url Page url for which we want to get dynfields

    @return dynfields json array of required dynfields, returned in sorted order
} {

	  array set query_hash $query_hash_pairs

      # Setting default values
	  set dynfields ""
      set result 0
      set error_msg ""
      set object_type_exist_p 0
      set page_url_exist_p 0

      # Checking if user provided required data       
      if {$object_type eq ""} {
          lappend error_msg "You need to provide 'object_type' parameter"
      } else {
          set object_type_exist_p 1
      }

      if {$page_url eq ""} {
          lappend error_msg "You need to provide 'page_url' parameter"
      } else {
          set page_url_exist_p 1
      }

      if {$object_type_exist_p eq 1 && $page_url_exist_p eq 1} {
          
          set result 1
          
          # We get dynfields for given object_type and page_url combination
          set attribute_sql "
             select aa.attribute_name, da.attribute_id, la.pos_x, la.pos_y, dw.widget, dw.parameters, aa.pretty_name, aa.pretty_plural
        	 from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la, im_dynfield_widgets dw
             where da.acs_attribute_id = aa.attribute_id
             and da.attribute_id = la.attribute_id
             and da.widget_name = dw.widget_name
             and la.page_url = :page_url
             and object_type = :object_type
             order by pos_y"

          set dynfields ""
          set obj_ctr 0

          # We now build array of widget (dynfield in form) parameters
          db_foreach dynfield $attribute_sql {
              set komma ",\n"
              set params ""

              if {[llength $parameters] > 0} {
                foreach param_cat $parameters {
                    set param_type [lindex $param_cat 0]
                    set param_values [lindex $param_cat 1]
                    db_0or1row dynfield_default_value "select distinct default_value as dynfield_default_value from im_dynfield_type_attribute_map where attribute_id=:attribute_id"
                    # Checking first three chars of dynfield_default_value, to see if we have 'tcl' in there (result of procedure)
                    set dynfield_default_value_ftl [string range $dynfield_default_value 0 2]
                    if {$dynfield_default_value_ftl eq "tcl"} { 
                       # Running procedure and setting what it returns as default value for dynfield
                       set dynfield_default_value [[lindex $dynfield_default_value 1]]
                    }
                    set obj_ctr_params 0
                    foreach {key val} $param_values {
                    	set komma_params ",\n"
                    	if {0 == $obj_ctr_params} { set komma_params "" }
                        append params "$komma_params{\"param_type\":\"$param_type\",\"param_name\":\"$key\", \"param_value\":\"$val\"}"
                        incr obj_ctr_params
                    }
                }
              }

              if {0 == $obj_ctr} { set komma "" }
              append dynfields "$komma{\"attribute_id\":$attribute_id, \"attribute_name\":\"$attribute_name\", \"pretty_name\":\"$pretty_name\", \"pretty_plural\":\"$pretty_plural\", \"default_value\":\"$dynfield_default_value\", \"pos_x\":\"$pos_x\", \"pos_y\":\"$pos_y\", \"widget\":\"$widget\", \"parameters\":\[\n$params\n\]\n}"
              incr obj_ctr
          }

      }

      set result "{\"success\": $result, \"error\":\"$error_msg\", \"page_url\":\"$page_url\", \n\"dynfields\": \[\n$dynfields\n\]\n}"
	  im_rest_doc_return 200 "application/json" $result
	  return	

}


ad_proc -public im_rest_get_custom_plugin {
    {-page_url ""}
    {-plugin_ids ""}
	{-return_url ""}
	{-project_id ""}
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Provide HTML of a component plugin
	
	Call this with /cognovis-rest/plugin?plugin_id=23882&company_id=8711
	
	So you can pass on all arguments as URL values

	@param plugin_ids ID of the plugin
	@param return_url URL to return, used in plugins
	@param page_url URL to use for plugins instead of plugin_ids
	@param project_id ID of the project for displaying the plugins for

	@return plugin_id ID of the plugin
	@return plugin_title Title shown for the plugin
	@return plugin_name Name of the plugin. Same as Title if the title does not contain a title_tcl
	@return plugin_html HTML of the plugin with URL replacements being done
	@return sort_order Number of the plugin on the location (Y-Position)
	@return location Location, typically one of left/right, but might also be top/buttom

} {
    set user_id $rest_user_id

	# Check we have valid plugin_ids    
    if {$plugin_ids eq ""} {
		set plugin_ids [db_list plugins "select plugin_id from im_component_plugins where page_url = :page_url and enabled_p = 't' order by location, sort_order asc"]
	    if {$plugin_ids eq ""} {
			im_rest_error -format "json" -http_status 403 -message "No Plugins found for $page_url"
    	}
    } else {
		set plugin_ids [db_list plugins "select plugin_id from im_component_plugins where plugin_id in ([template::util::tcl_to_sql_list $plugin_ids]) order by location, sort_order asc"]
		if {$plugin_ids eq ""} {
			im_rest_error -format "json" -http_status 403 -message "Problem calling for plugins. No valid plugins found."
		}
    }

    # Loop through each plugin
    set plugin_json [list]
    
    foreach plugin_id $plugin_ids {
    	if {![db_0or1row compoent "select * from im_component_plugins where plugin_id =:plugin_id"]} {
        	im_rest_error -format "json" -http_status 403 -message "Problem finding the pluging $plugin_id"
        }

		if {$title_tcl eq ""} {
		    set plugin_title $plugin_name
		} else {
		    if {[catch $title_tcl plugin_title]} {
				im_rest_error -format "json" -http_status 403 -message "Problem Getting the title $title_tcl :: $title"
		    }
		}

		if {[catch $component_tcl plugin_html]} {
		    im_rest_error -format "json" -http_status 403 -message "Problem calling $component_tcl ::: $plugin_html"
		}

		# Regsub common missing urls
		regsub -all {HREF=/} $plugin_html "target=_blank HREF=[ad_url]/" plugin_html
		regsub -all {HREF=\"/} $plugin_html "target=_blank HREF=\"[ad_url]/" plugin_html
		regsub -all {HREF='/} $plugin_html "target=_blank HREF='[ad_url]/" plugin_html
		regsub -all {action=/} $plugin_html "target=_blank action=[ad_url]/" plugin_html
		regsub -all {action=\"/} $plugin_html "target=_blank action=\"[ad_url]/" plugin_html
		regsub -all {action='/} $plugin_html "target=_blank action='[ad_url]/" plugin_html
		regsub -all {href=/} $plugin_html "target=_blank href=[ad_url]/" plugin_html
		regsub -all {href=\"/} $plugin_html "target=_blank href=\"[ad_url]/" plugin_html
		regsub -all {href='/} $plugin_html "target=_blank href='[ad_url]/" plugin_html
		regsub -all {src=\"/} $plugin_html "src=\"[ad_url]/" plugin_html
		regsub -all {src=/} $plugin_html "src=[ad_url]/" plugin_html
		regsub -all {src='/} $plugin_html "src='[ad_url]/" plugin_html

		lappend plugin_json [im_rest_json_object]

    }

    set result "{\"success\": true,\n\"total\": [llength $plugin_json],\n\"message\": \"$plugin_name: Plugin Data loaded\",\n\"data\": \[ \n
        [join $plugin_json ", \n"]
    \] \n}"
	im_rest_doc_return 200 "application/json" $result
	return
}
