# /packages/intranet-sencha-tables-rest-procs

ad_library {
	Rest Procedures for the sencha-tables package
	
	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_rest_get_custom_sencha_reporting_locked {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting information on locked segments
} {
	
	set permission_p [im_permission $rest_user_id "view_projects_all"]
	if {!$permission_p} {return}
	
	set sql {
		select im_name_from_id(company_id) as object_name, im_name_from_id(company_id) as company_name,
			company_id as object_id, company_id,
			(select count(*) from im_projects p2 where p2.company_id = p.company_id) as sum_projects,
			count(company_id) as sum_locked_projects,
			sum(locked_segments) as sum_locked_segments,
			round(sum(billable_units),0) as sum_billable_units,
			to_char(p.start_date,'MM') as mm,
			extract(year from p.start_date) as yyyy
		from im_projects p, im_trans_tasks t,
			 (select project_id, sum(locked+match_lock) as locked_segments from im_trans_tasks where locked >0 or match_lock >0 group by project_id) l
		where l.project_id = p.project_id and t.project_id = p.project_id group by company_id, p.start_date
	}
	
	im_sencha_tables_rest_call \
		-valid_vars [list company_id company_name sum_projects sum_locked_projects sum_locked_segments sum_billable_units mm yyyy] \
		-sql $sql \
		-order_by sum_locked_projects \
		-query_hash_pairs $query_hash_pairs \
		-rest_user_id $rest_user_id
}


ad_proc -public im_rest_get_custom_sencha_reporting_productive_hours {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting information on employees hours
	
	Use group_by in the JSON to define by which variable we should filter (typically subject_area_id vs. project_type_id)
	Returns a JSON with
	- Employee_id and name
	- Subject_area
	- productive hours = hours spend on projects with an invoice
	- unproductive hours = any other hour
} {

	set permission_p [im_permission $rest_user_id "view_projects_all"]
	if {!$permission_p} {return}
	
	array set query_hash $query_hash_pairs
	
	if {[info exists query_hash(group_by)]} {
		set group_by $query_hash(group_by)
	} else {
		set group_by "project_type_id"
	}

	set sql "
		 select case when $group_by is not null then user_id::text || ${group_by}::text || mm || yyyy else user_id::text || mm || yyyy end as object_id, user_id as employee_id, im_name_from_id(user_id) as object_name,
		 	im_name_from_id($group_by) as group_by_name,
	    $group_by as group_by_id,
		 	sum(p_hours) as productive_hours, sum(u_hours) as unproductive_hours,
		 	mm, yyyy
		 from (
			  select to_char(p2.start_date,'MM') as mm,
			  	extract(year from p2.start_date) as yyyy,
			  	p1.user_id, sum(p1.hours) as p_hours, 0 as u_hours,
			  	p1.project_id, p2.$group_by
			  from (select sum(h.hours) as hours, parent_p.project_id, user_id
	from   im_projects p, im_projects parent_p, im_hours h
where  parent_p.project_id in (select distinct project_id from im_costs where cost_type_id in (3700,3725) and project_id is not null)
and    p.tree_sortkey between parent_p.tree_sortkey and tree_right(parent_p.tree_sortkey)
and h.project_id = p.project_id group by parent_p.project_id, h.user_id) p1, im_projects p2
			  where p2.project_id = p1.project_id
			  group by user_id, p1.project_id, $group_by, start_date
			 UNION
			  select to_char(p2.start_date,'MM') as mm,
  			  	extract(year from p2.start_date) as yyyy,
  			  	p1.user_id, 0 as p_hours, sum(p1.hours) as u_hours,
			  			  	p1.project_id, p2.$group_by
			  			  from (select sum(h.hours) as hours, parent_p.project_id, user_id
			  	from   im_projects p, im_projects parent_p, im_hours h
			  where  parent_p.project_id not in (select distinct project_id from im_costs where cost_type_id in (3700,3725) and project_id is not null)
			  and    p.tree_sortkey between parent_p.tree_sortkey and tree_right(parent_p.tree_sortkey)
			  and h.project_id = p.project_id group by parent_p.project_id, h.user_id) p1, im_projects p2
  			  where p2.project_id = p1.project_id
 			  group by user_id, p1.project_id, $group_by, start_date
		) sl
		group by user_id, $group_by, mm, yyyy
	"

	im_sencha_tables_rest_call \
		-valid_vars [list group_by_name group_by_id employee_id productive_hours unproductive_hours yyyy mm] \
		-sql $sql \
		-order_by user_id \
		-query_hash_pairs $query_hash_pairs \
		-rest_user_id $rest_user_id
}

ad_proc -public im_rest_get_custom_sencha_reporting_offer_hitrate {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for returing the turnover data for projects based on the project creation date

	Returns a JSON with
	- Projekt Type
	- Month (based on effective date, im_costs)
	- # Num Offers
	- # Num accepted offers
	- Sum offer revenue (total)
	- Sum offer revenue (accepted offers)
	- Hit rate (#accepted offers / #unacc_offers)
	
} {

	set permission_p [im_permission $rest_user_id "view_projects_all"]
	if {!$permission_p} {return}
	
	set sql {
		select cost_center_id || mm || yyyy as object_id, cost_center_name as object_name,
		m.*, acc_offers+unacc_offers as total_offers, acc_amount+unacc_amount as total_amount,
		round(acc_offers/(acc_offers+unacc_offers),3)*100 as hit_rate from (
			select mm, yyyy, im_cost_center_name_from_id(cost_center_id) as cost_center_name, cost_center_id,
				sum(acc_offers) as acc_offers, sum(acc_offer_amount) as acc_amount,
				sum(unacc_offers) as unacc_offers, sum(unacc_amount) as unacc_amount
			from (
				select to_char(effective_date,'MM') as mm,
					extract(year from effective_date) as yyyy,
					cost_center_id,
					count(cost_id) as acc_offers, sum(amount) as acc_offer_amount,
					0 as unacc_offers, 0 as unacc_amount
				from im_costs
				where cost_type_id = 3702
					and cost_status_id in (3810,3820)
				group by cost_center_id, effective_date
				union
				select to_char(effective_date,'MM') as mm,
					extract(year from effective_date) as yyyy,
					cost_center_id,
					0 as acc_offers, 0 as acc_amount,
					count(cost_id) as unacc_offers, sum(amount) as unacc_offer_amount
				from im_costs
				where cost_type_id = 3702
				and cost_status_id not in (3810,3820,3812,3813)
				group by cost_center_id, effective_date
			) s group by yyyy, mm, cost_center_id
		) m
	}

	im_sencha_tables_rest_call \
		-valid_vars [list yyyy mm cost_center_name cost_center_id acc_offers unacc_offers hit_rate total_offers total_amount unacc_offers unacc_amount] \
		-sql $sql \
		-order_by yyyy,mm,cost_center_name \
		-query_hash_pairs $query_hash_pairs \
		-rest_user_id $rest_user_id
}

ad_proc -public im_rest_get_custom_sencha_reporting_project_hitrate {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for returing the turnover data for projects based on the project creation date

	Returns a JSON with
	- Projekt Type
	- Month (based on effective date, im_costs)
	- # Num created Projects
	- # Num accepted Projects (either open or delivered)
	- Sum offer revenue (total, not filed or declined)
	- Sum offer revenue (accepted offers)
	- Hit rate (#accepted projects / #unacc_projects)

} {

	set permission_p [im_permission $rest_user_id "view_projects_all"]
	if {!$permission_p} {return}


	array set query_hash $query_hash_pairs
	
	if {[info exists query_hash(group_by)]} {
		set group_by $query_hash(group_by)
		
		# Can't filter by elements in the where clause
		if {$group_by eq "project_status_id"} {
			set group_by "project_type_id"
		}
	} else {
		set group_by "project_type_id"
	}

	set accepted_project_type_ids [im_sub_categories [list [im_project_status_open] [im_project_status_delivered] [im_project_status_invoiced]]]

	set sql "
		select m.$group_by || mm || yyyy as object_id, m.$group_by as group_by_id, im_name_from_id(m.$group_by) as object_name, im_name_from_id(m.$group_by) as group_by_name,
		m.*, acc_projects+unacc_projects as total_projects, acc_amount+unacc_amount as total_amount,
		round(acc_projects/(acc_projects+unacc_projects),3)*100 as hit_rate from (
			select mm, yyyy, $group_by,
				sum(acc_projects) as acc_projects, sum(acc_offer_amount) as acc_amount,
				sum(unacc_projects) as unacc_projects, sum(unacc_amount) as unacc_amount
			from (
				select to_char(end_date,'MM') as mm,
					extract(year from end_date) as yyyy,
					$group_by,
					count(project_id) as acc_projects, sum(cost_quotes_cache) as acc_offer_amount,
					0 as unacc_projects, 0 as unacc_amount
				from im_projects
				where project_status_id in ([template::util::tcl_to_sql_list $accepted_project_type_ids])
                                and project_type_id not in (100,101)
				group by $group_by, end_date
				union
				select to_char(end_date,'MM') as mm,
					extract(year from end_date) as yyyy,
					$group_by,
					0 as acc_projects, 0 as acc_amount,
					count(project_id) as unacc_projects, sum(cost_quotes_cache) as unacc_offer_amount
				from im_projects
				where project_status_id not in ([template::util::tcl_to_sql_list $accepted_project_type_ids])
                                and project_type_id not in (100,101)
				group by $group_by, end_date
			) s group by yyyy, mm, $group_by
		) m
	"
	
	if {$group_by eq "company_id"} {
		append sql ", im_companies c where c.company_id = m.company_id and c.company_status_id = [im_company_status_active]"
	}

	im_sencha_tables_rest_call \
		-valid_vars [list yyyy mm group_by_id group_by_name acc_projects unacc_projects hit_rate total_projects total_amount acc_amount unacc_amount] \
		-sql $sql \
		-order_by yyyy,mm,$group_by \
		-query_hash_pairs $query_hash_pairs \
		-rest_user_id $rest_user_id
}
