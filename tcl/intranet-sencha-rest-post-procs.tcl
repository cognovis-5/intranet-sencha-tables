ad_library {
    Rest POST Procedures for the sencha-tables package
    
    @author malte.sussdorff@cognovis.de
}




# ---------------------------------------------------------------
# ERROR HANDLING!!!!
# This is missing throughout, so we need to properly catch
# And return an error message to the user....
# ---------------------------------------------------------------

namespace eval sencha_errors {

    variable objects_warnings ""
    variable objects_errors ""
    variable existing_ids [list]


    ad_proc add_error {
        -object_id
        {-field ""}
        -problem
    } {
       adding an error, allowing commulation of errors for later provision with display_errors_and_warnings
       @param object_id Object which has the problem. if there is no object_id yet, use a random generated token
    } {
        variable objects_errors
        variable existing_ids

        lappend existing_ids $object_id
        dict lappend objects_errors $object_id $field $problem
    }

    ad_proc add_warning {
        -object_id
        {-field ""}
        -problem
    } {
       adding a warning, allowing commulation of warnings for later provision with display_errors_and_warnings
       @param object_id Object which has the problem. if there is no object_id yet, use a random generated token
    } {
        variable objects_warnings
        variable existing_ids

        lappend existing_ids $object_id
        dict lappend objects_warnings $object_id $field $problem
    }

    ad_proc clear {
        -object_id
    }  {
       clearing commulated errors and warnings for given @param object_id
    } {
        variable objects_warnings
        variable objects_errors
        set objects_warnings ""
        set objects_errors ""
    }

    ad_proc display_errors_and_warnings {
        -object_id
        {-action_type ""}
        {-modal_title ""}
    } {
        displaying errors and warnings for given @param object_id in case any problems appeared
    } {

        variable objects_warnings
        variable objects_errors
        variable existing_ids
        
        set obj_ctr_errors 0
        set obj_ctr_warnings 0
        set json_errors ""
        set json_warnings ""
        
        # building json structure for warnings
        set size_of_warnings [dict size $objects_warnings]
        if {$size_of_warnings > 0} {
            set single_object_warnings [dict get $objects_warnings $object_id]
            dict for {field warning} $single_object_warnings {
                set komma ",\n"
                if {0 == $obj_ctr_warnings} { set komma "" }
                if {$field ne ""} {
                    append json_warnings "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $warning]\"}"
                } else {
                    append json_warnings "$komma{\"message\": \"[im_quotejson $warning]\"}"
                }
                incr obj_ctr_warnings
            }
        }
       # building json structure for errors
       set size_of_errors [dict size $objects_errors]
       if {$size_of_errors > 0} {
           set single_object_errors [dict get $objects_errors $object_id]
           dict for {field error} $single_object_errors {
               set komma ",\n"
               if {0 == $obj_ctr_errors} { set komma "" }
               if {$field ne ""} {
                   append json_errors "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $error]\"}"
                } else {
                   append json_errors "$komma{\"message\": \"[im_quotejson $error]\"}"
                }
               incr obj_ctr_errors
           }
       }

        #clearing errros and warnings before possible display
        clear -object_id $object_id
        set existing_ids [list]
        if {$size_of_errors > 0 || $size_of_warnings > 0} {
            set result "{\"success\": false, \"object_id\": \"$object_id\", \"action_type\": \"$action_type\", \"modal_title\": \"$modal_title\", \n\"message\": \"\",\n\"warnings\": \[\n$json_warnings\n\],\n\"errors\": \[\n$json_errors\n\]\n}"
            im_rest_doc_return 200 "application/json" $result
            return
            ad_script_abort
        }
        
    }

    ad_proc display_all_existing_errors_and_warnings {
        -object_ids
        {-action_type ""}
        {-modal_title ""}
    } {
        displaying errors and warnings for given @param object_ids (list) in case any problems appeared
    } {

        variable objects_warnings
        variable objects_errors
        
        set obj_ctr_errors 0
        set obj_ctr_warnings 0
        set json_errors ""
        set json_warnings ""

        set size_of_warnings 0
        set size_of_errors 0



        foreach object_id $object_ids {

            # building json structure for warnings
            set size_of_warnings [dict size $objects_warnings]
            if {$size_of_warnings > 0} {
                set single_object_warnings [dict get $objects_warnings $object_id]
                dict for {field warning} $single_object_warnings {
                    set komma ",\n"
                    if {0 == $obj_ctr_warnings} { set komma "" }
                    if {$field ne ""} {
                        append json_warnings "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $warning]\"}"
                    } else {
                        append json_warnings "$komma{\"message\": \"[im_quotejson $warning]\"}"
                    }
                    incr obj_ctr_warnings
                }
            }
           # building json structure for errors
           set size_of_errors [dict size $objects_errors]
           if {$size_of_errors > 0} {
               set single_object_errors [dict get $objects_errors $object_id]
               dict for {field error} $single_object_errors {
                   set komma ",\n"
                   if {0 == $obj_ctr_errors} { set komma "" }
                   if {$field ne ""} {
                       append json_errors "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $error]\"}"
                    } else {
                       append json_errors "$komma{\"message\": \"[im_quotejson $error]\"}"
                    }
                   incr obj_ctr_errors
               }
           }

        }

        #clearing errros and warnings before possible display
        if {$size_of_errors > 0 || $size_of_warnings > 0} {

            set object_ids [list]
            set result "{\"success\": false, \"object_id\": \"$object_id\", \"action_type\": \"$action_type\", \"modal_title\": \"$modal_title\", \n\"message\": \"\",\n\"warnings\": \[\n$json_warnings\n\],\n\"errors\": \[\n$json_errors\n\]\n}"
            im_rest_doc_return 200 "application/json" $result

            # Clear
            foreach object_id $object_ids {
                clear -object_id $object_id
            }

            return
            ad_script_abort
        }
        
    }

}


# ---------------------------------------------------------------
# Projects endpoint
# ---------------------------------------------------------------

ad_proc -public im_rest_post_object_type_sencha_project {
    -company_id
    -project_type_id
    -source_language_id
    -target_language_ids
    { -subject_area_id ""}
    { -final_company_id ""}
    { -customer_contact_id ""}
    { -uploadedFiles ""}
    { -projectTasks ""}
    { -language_experience_level_id ""}
    { -project_id ""}
    { -project_name ""}
    { -project_nr ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -debug 0 }
    { -query_hash_pairs ""}
    { -do_analysis_p 0 }
} {
    Handler for POST calls on the project. Will use any variable passed through the query_hash_pairs to create / update the project
    if the variable has been defined as attributes for the object_type.

    For variables starting with "skill_", these will be added as freelancer skills to the project. So are source_language_id, target_language_ids, subject_area_id and
    language_experience_level_id (the level for the skills in languages).

    @param company_id Company in which to create the project
    @param project_type_id Project Type of the project to create
    @param source_language_id Source Language for the project. Typically the language the source material is in
    @param target_language_ids Target Languages into which to translate the source material
    @param subject_area_id Subject Area of the project (determines pricing among other things)
    @param final_company_id Final company for this translation (determines the reference material and Termbase / Translation Memory to use)
    @param customer_contact_id Contact in the company who requested this project
    @param uploadedFiles Path of files which were uploaded previously for this project
    @param projectTasks Array of tasks
    @param language_experience_level_id Required experience level for this project. Used (previously) to differentiate certified translations
    @param project_id ID of the project. Useful if you edit instead of create a new project
    @param project_name Name of the project
    @param project_nr Number of the project. Mostly automatically determined now though
    @param query_harsh_pairs Pairs of variables passed onto the submit form not defined as explicit switches
    @oaram do_analysis_p Switch which decided if we should execute MemoQ file analysis. 1 = Yes, 0 = No.
} {
    ns_log Notice "im_rest_post_sencha_project: rest_oid=$rest_oid with query_hash $query_hash_pairs"
   
    # Extract a key-value list of variables from JSON POST request
    set random_errors_token "[ad_generate_random_string]"
  
    #-------------------------------------------------------
    # Check if we are creating new project or editing existing
    set project_exists_p 0
    if {[exists_and_not_null project_id]} {
    if {[ad_var_type_check_number_p $project_id]} {
        set project_exists_p [db_string project_exists "
                    select count(*)
                    from im_projects
                    where project_id = :project_id
            "]
    } else {
        set project_name $project_id
    }
    }
    
    if {$project_exists_p eq 1} {
    
        set sql "update im_projects set"
    
        if {[exists_and_not_null final_company_id]} {
            append sql " final_company_id=:final_company_id,\n"
        }
        if {[exists_and_not_null company_id]} {
            append sql " company_id=:company_id,\n"
        }
        if {[exists_and_not_null project_type_id]} {
            append sql " project_type_id=:project_type_id,\n"
        }
        if {[exists_and_not_null project_nr]} {
            append sql " project_nr=:project_nr,\n"
        }
        if {[exists_and_not_null project_name]} {
            append sql " project_name=:project_name,\n"
        }
        if {[exists_and_not_null customer_contact_id]} {
            append sql " company_contact_id=:customer_contact_id,\n"
        }
        if {[exists_and_not_null expected_quality_id]} {
            append sql "expected_quality_id=:expected_quality_id,\n"
        }
        if {[exists_and_not_null subject_area_id]} {
            append sql " subject_area_id=:subject_area_id,\n"
        }
        if {[exists_and_not_null source_language_id]} {
        append sql " source_language_id=:source_language_id,\n"
        }
    
        append sql " project_id=:project_id where project_id=:project_id"
    
        set target_language_ids [split $target_language_ids ","]
        db_transaction {
        db_dml update_im_projects $sql
        db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
        }
    
        db_transaction {
            foreach target_language_id $target_language_ids {
                db_dml insert_im_target_language "insert into im_target_languages values ($project_id, $target_language_id)"
            }
        }
    
        if {[apm_package_installed_p "intranet-freelance"]} {
        
        db_dml delete_exising_skills "delete from im_object_freelance_skill_map where object_id =:project_id"
        
        #later we need to have possibiliy to return 2028 id by something like [im_freelance_skill_type_subject_area]
        if {$language_experience_level_id ne ""} {
        im_freelance_add_required_skills -object_id $project_id -skill_type_id 2028 -skill_ids $language_experience_level_id
        }
            
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
        #im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_expected_quality] -skill_ids $expected_quality_id
        
        foreach target_language_id $target_language_ids {
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $target_language_id
        }
    }
    } else {
        
       sencha_errors::display_errors_and_warnings -object_id $random_errors_token
      
       set project_id [im_translation_create_project \
               -company_id $company_id \
               -project_name $project_name \
               -project_nr $project_nr \
               -project_type_id $project_type_id \
               -project_lead_id [ad_conn user_id] \
               -source_language_id $source_language_id \
               -target_language_ids $target_language_ids \
               -subject_area_id $subject_area_id \
               -final_company_id $final_company_id \
               -no_callback]
       
       if {$project_id eq 0} {
       # There was  an error creating the project, try to find out if it is because it was a duplicate
       set project_id [db_string project_id "select project_id from im_projects where
            (   upper(trim(project_name)) = upper(trim(:project_name)) OR
                upper(trim(project_nr)) = upper(trim(:project_nr)) OR
                upper(trim(project_path)) = upper(trim(:project_name))
            )" -default 0]
       
       ns_log Notice "Tried to find the project ... $project_id .. $project_nr"
       if {$project_id eq 0} {
           set project_id $random_errors_token
           set error_msg "Failed to create project. We sadly don't know why"
           sencha_errors::add_error -object_id $random_error_token -problem $error_msg
           set redirect_url "/intranet"
       } else {
           set error_msg "Found project already existing"
           sencha_errors::add_warning -object_id $project_id -problem $error_msg
       }
       
       } else {
       
       # Set project status to potential
       db_dml update_project_status "update im_projects set project_status_id = [im_project_status_potential], company_contact_id = :customer_contact_id where project_id = :project_id"
       
       # Update project with values found in the hash array
       im_rest_object_type_update_sql \
           -rest_otype im_project \
           -rest_oid $project_id \
           -hash_array $query_hash_pairs
       
       # ---------------------------------------------------------------
       # Make sure we have the skills
       # ---------------------------------------------------------------
       if {[apm_package_installed_p "intranet-freelance"]} {
           
           # ---------------------------------------------------------------
           # We need to store source and target language again
           # ---------------------------------------------------------------
           
           #later we need to have possibiliy to return 2028 id by something like [im_freelance_skill_type_subject_area]
           if {$language_experience_level_id ne ""} {
           im_freelance_add_required_skills -object_id $project_id -skill_type_id 2028 -skill_ids $language_experience_level_id
           }
           
           im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
           
           foreach key [array names hash_array] {
           if {[string match "skill_*" $key]} {
               set skill_type_id [lindex [split $key "_"] 1]
               im_freelance_add_required_skills -object_id $project_id -skill_type_id $skill_type_id -skill_ids  $hash_array($key)
           }
           }
       }
       
       # We need to catch the audit, as the project might still be created
       
        catch {
            cog::callback::invoke -object_type "im_project" -object_id $project_id -type_id $project_type_id -status_id [im_project_status_potential] -action after_create
        }
       
       # ---------------------------------------------------------------
       # Deal with the uploaded files array
       # ---------------------------------------------------------------
       if {[exists_and_not_null uploadedFiles]} {
           set uploaded_files_original $uploadedFiles
           set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
           set project_dir [im_filestorage_project_path $project_id]
           set source_dir "${project_dir}/$source_folder"
           foreach uploadedFiles [lindex $uploadedFiles 1] {
           array set one_file [lindex $uploadedFiles 1]
           file rename -force $one_file(path) "${source_dir}/$one_file(file)"
           }
           
           #callbacks to MemoQ & Trados
           if {$do_analysis_p == 1} {
               catch {callback im_project_after_file_upload -project_id $project_id -type_id $project_type_id -status_id [im_project_status_potential]}
               ns_log Notice "Yes for MemoQ analysis"
           } else {
               ns_log Notice "No for MemoQ analysis"
           }
           
           foreach file [lindex $uploaded_files_original 1] {
           array set one_file [lindex $file 1]
           set task_filename $one_file(file)
           set task_uom_id [im_uom_s_word] 
           set count_created_tasks [db_string count_created_tasks "select count(*) from im_trans_tasks where project_id = :project_id and task_filename =:task_filename " -default 0]
           if {$count_created_tasks eq 0} {
               ns_log Notice "DEBUGING inserted $task_filename for project: $project_id"
               im_task_insert $project_id $task_filename $task_filename 0 $task_uom_id $project_type_id $target_language_ids
           }
           }
           set create_quote_p [db_string tasks_exist "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]
       }
       
       # ---------------------------------------------------------------
       # Create tasks if necessary
       # ---------------------------------------------------------------
       if {[exists_and_not_null projectTasks]} {
           foreach projectTask [lindex $projectTasks 1] {
           array unset one_task
           array set one_task [lindex $projectTask 1]
           
           if {[exists_and_not_null one_task(units_of_measure)]} {
               set task_uom_id $one_task(units_of_measure)
           } else {
               set task_uom_id 324 ;# Source words
           }
           set task_status_id 340
           
           if {[exists_and_not_null one_task(name)]} {
               # Insert into the project
               set ip_address [ad_conn peeraddr]
               if {[exists_and_not_null one_task(description)]} {
               set description $one_task(description)
               } else {
               set description ""
               }
               
               if {[exists_and_not_null one_task(units)]} {
               set billable_units $one_task(units)
               } else {
               set billable_units ""
               }
               set task_name $one_task(name)
               
               foreach target_language_id $target_language_ids {
               
               set new_task_id [im_exec_dml new_task "im_trans_task__new (
                                    null,                   -- task_id
                                    'im_trans_task',        -- object_type
                                    now(),                  -- creation_date
                                    :rest_user_id,               -- creation_user
                                    :ip_address,            -- creation_ip
                                    null,                   -- context_id
                        
                                    :project_id,            -- project_id
                                    :project_type_id,          -- task_type_id
                                    :task_status_id,        -- task_status_id
                                    :source_language_id,    -- source_language_id
                                    :target_language_id,    -- target_language_id
                                    :task_uom_id            -- task_uom_id
                            )"]
               
               if {[catch {db_dml update_task "update im_trans_tasks set
                                description = :description,
                                task_name = :task_name,
                                task_units = :billable_units,
                                billable_units = :billable_units
                            WHERE
                            task_id = :new_task_id"}]} {
                   # Most likely the task name was provided twice, create with copy
                   set task_name "$task_name (copy)"
                   db_dml update_task "update im_trans_tasks set
                                description = :description,
                                task_name = :task_name,
                                task_units = :billable_units,
                                billable_units = :billable_units
                            WHERE
                            task_id = :new_task_id"
               }
               set create_quote_p 1
               }
           }
           }
       }

       set redirect_url [export_vars -base "/intranet/projects/view" -url {project_id}]
       if {$create_quote_p} {
           set quote_id [im_trans_invoice_create_from_tasks -project_id $project_id -cost_type_id [im_cost_type_quote]]
           set redirect_url [im_biz_object_url $quote_id]
           set amount_sql "select sum(price_per_unit * item_units) from im_invoice_items where invoice_id =:quote_id"
           set quote_amount [db_string invoice_amount $amount_sql]
           if {$quote_amount > 0} {
           set return_url [export_vars -base "/intranet/projects/view" -url {project_id}]
           set redirect_url "[ad_url][apm_package_url_from_key "intranet-invoices"]view?invoice_id=$quote_id&return_url=$return_url"
           }
       }
       
       # Calculate the end date
       set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
       im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp
       }
   }
    
    set hash_array(rest_oid) "$project_id"
    #set hash_arry(project_id) "$project_id"
    
    if {![info exists redirect_url]} {
    set redirect_url [export_vars -base "/intranet/projects/view" -url {project_id}]
    }
    set hash_array(redirect_url) $redirect_url
    return [array get hash_array]
}

# ---------------------------------------------------------------
# Company Adding endpoint
# ---------------------------------------------------------------

ad_proc -public im_rest_post_object_type_sencha_company {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on companies.
    Can either create new company (with user) and edit existing one

    @param company_id Id of company in case we are editing existing one
    @param first_names First name of user in case we create new company
    @param last_name Last name of user in case we create new company
    @param email Email of user in case we create new company
    @param company_type_id Type of company
    @param country Country of company main_office_id
    @param streeet Street (address_line_1) of company main office
    @param city of company main office
    @zipcode zip code / postal code of company main office
    @website company website
    @vat_number company vat number
    @iban company iban
    @bic company bic
    @paypal company paypal email
    @skrill company skrill email
    @salutation company salutation used in email templates
    @mobile_phone mobile phone of user
    @fax company fax
    @skype  company skype
    @proz_url company proz_url
} {
    ns_log Notice "im_rest_post_sencha_company: rest_oid=$rest_oid"

    # Permissions
    # ToDo

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]


    # Check that all required variables are there
    set random_errors_token "[ad_generate_random_string]"
    set required_vars [list first_names last_name email]
    foreach var $required_vars {
        if {![info exists hash_array($var)] || $hash_array($var) eq ""} {
            sencha_errors::add_error -object_id $random_errors_token -field $var -problem "is required"
        } else {
            set $var $hash_array($var)
        }
    }
    
    set optional_vars [list company_id company_name company_type_id country street city country zipcode website vat_number iban bic paypal skrill salutation phone mobile_phone fax skype proz_url]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }

    sencha_errors::display_errors_and_warnings -object_id $random_errors_token

    # ------------------------------------------------------------------
    # Permissions
    # ------------------------------------------------------------------
       
    # Check if we are creating a new company or editing an existing one:
    set company_exists_p 0
    if {$company_id ne ""} {
        if {[ad_var_type_check_number_p $company_id]} {
            set company_exists_p [db_string company_exists "
                    select count(*)
                    from im_companies
                    where company_id = :company_id
            "]
            if {$company_exists_p} {
                db_1row existing_company_info "select company_name from im_companies where company_id = :company_id"
            }
        } else {
            set company_name $company_id
        }
    }

    regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
    
    if {$company_exists_p eq 0} {
        # Check if we have the path
        set company_id [db_string company_exists "select company_id from im_companies where company_path = :company_path" -default 0]
        if {$company_id ne 0} {
            set company_exists_p 1
        }
    }


    if {$company_exists_p} {
        if {[im_user_main_company_id -user_id $rest_user_id] ne $company_id} {
            # Check company permissions for this user
            im_company_permissions $rest_user_id $company_id view read write admin      
        } else {
            set write 1
        }
        if {!$write} {
            #return [im_rest_error -format $format -http_status 406 -message "[_ intranet-core.lt_Insufficient_Privileg] [_ intranet-core.lt_You_dont_have_suffici]"
            sencha_errors::add_error -object_id $random_errors_token -problem "[_ intranet-core.lt_Insufficient_Privileg] [_ intranet-core.lt_You_dont_have_suffici]"
            sencha_errors::display_errors_and_warnings -object_id $random_errors_token -action_type "show_modal_and_reload" -modal_title "Insufficient Privileges"
        }
    } else {
        if {![im_permission $rest_user_id add_companies]} {
            sencha_errors::add_error -object_id $random_errors_token -problem "[_ intranet-core.lt_Insufficient_Privileg] [_ intranet-core.lt_You_dont_have_suffici]"
            sencha_errors::display_errors_and_warnings -object_id $random_errors_token -action_type "show_modal_and_reload" -modal_title "Insufficient Privileges"
        }
   }
   
   
    # ---------------------------------------------------------------
    # Default Values
    # ---------------------------------------------------------------
    set profile [im_profile_customers]
    if {$company_type_id == [im_company_type_customer]} {set profile [im_profile_customers]}
    if {$company_type_id == [im_company_type_partner]} {set profile [im_profile_partners]}
    if {$company_type_id == [im_company_type_provider]} {set profile [im_profile_freelancers]}
    if {$company_type_id == [im_company_type_internal]} {set profile [im_profile_employees]}

    set office_path "${company_path}_main_office"
    set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}]"

    if {![info exists email]} { set email "" }
    set username [string tolower $email]
    if {![info exists first_names]} { set first_names $first_name}
    set screen_name "$first_names $last_name"
    set url $website
    set secret_question ""
    set secret_answer ""
    
    if {!$company_exists_p} {
        set company_id [im_new_object_id]
        set office_id [im_new_object_id]
        set company_status_id [im_company_status_active]
        set office_type_id [im_office_type_main]
    } else {
        db_1row office_info "select office_id, main_office_id, company_status_id, office_type_id from im_companies c, im_offices o where o.office_id = c.main_office_id and c.company_id = :company_id"
    }

    # -----------------------------------------------------------------
    # Create user
    # -----------------------------------------------------------------
       
    set user_id [db_string first_last_name_exists_p "
        select  user_id
        from    cc_users
        where   lower(trim(first_names)) = lower(trim(:first_names)) and
            lower(trim(last_name)) = lower(trim(:last_name)) and
            lower(trim(email)) = lower(trim(:email))
        " -default ""]

       
    if {"" == $user_id} {

        # New user: create from scratch
        set email [string trim $email]
        set similar_user [db_string similar_user "select party_id from parties where lower(email) = lower(:email)" -default 0]
    
        if {$similar_user > 0} {
            set view_similar_user_link "<A href=/intranet/users/view?user_id=$similar_user>[_ intranet-core.user]</A>"
            sencha_errors::add_error -object_id $company_id -field "email" -problem "[_ intranet-core.Duplicate_UserB][_ intranet-core.lt_There_is_already_a_vi]"
            sencha_errors::display_errors_and_warnings -object_id $company_id
        }
    
        set password [ad_generate_random_string]
        set password_confirm $password



        array set creation_info [auth::create_user \
                     -user_id $user_id \
                     -verify_password_confirm \
                     -username $username \
                     -email $email \
                     -first_names $first_names \
                     -last_name $last_name \
                     -screen_name $screen_name \
                     -password $password \
                     -password_confirm $password_confirm \
                     -url $url \
                     -secret_question $secret_question \
                     -secret_answer $secret_answer]

        set creation_status "error"
        if {[info exists creation_info(creation_status)]} { set creation_status $creation_info(creation_status)}
        if {"ok" != [string tolower $creation_status]} {
            set countElements [llength $creation_info(element_messages)]
            set numberOfLoops [expr $countElements / 2]
            set errorArr ""
            for {set i 0} {$i < $numberOfLoops } {incr i} {
               set j [expr {$i * 2}]
               set var1 [lindex $creation_info(element_messages) $j]
               set var2 [lindex $creation_info(element_messages) $j+1]
               sencha_errors::add_error -object_id $company_id -field $var1 -problem $var2
            }
            sencha_errors::display_errors_and_warnings -object_id $company_id
        }
    
        # Extract the user_id from the creation info
        if {[info exists creation_info(user_id)]} {
            set user_id $creation_info(user_id)
            set role_id [im_biz_object_role_full_member]
        } else {
            sencha_errors::add_error -object_id $company_id  -problem [lindex $creation_info(element_messages) 0]
            sencha_errors::display_errors_and_warnings -object_id $company_id
        }
    
        # Update creation user to allow the creator to admin the user
        db_dml update_creation_user_id "
            update acs_objects
            set creation_user = :rest_user_id
            where object_id = :user_id
        "
    
        # Add the user to a group.
        # Check whether the rest_user_id has the right to add the guy to the group:
        set managable_profiles [im_profile::profile_options_managable_for_user $rest_user_id]
        foreach profile_tuple $managable_profiles {
            set profile_name [lindex $profile_tuple 0]
            set profile_id [lindex $profile_tuple 1]
            if {$profile == $profile_id} { im_profile::add_member -profile_id $profile -user_id $user_id }
        }
    
    } else {
    
        # Existing user: Update variables
        set auth [auth::get_register_authority]
        set user_data [list]
    
        # Make sure the "person" exists.
        # This may be not the case when creating a user from a party.
        set person_exists_p [db_string person_exists "select count(*) from persons where person_id = :user_id"]
        if {!$person_exists_p} {
            db_dml insert_person "
                insert into persons (
                    person_id, first_names, last_name
                ) values (
                    :user_id, :first_names, :last_name
                )
            "
            # Convert the party into a person
            db_dml person2party "
                update acs_objects
                set object_type = 'person'
                where object_id = :user_id
            "
        }
    
        set user_exists_p [db_string user_exists "select count(*) from users where user_id = :user_id"]
        if {!$user_exists_p} {
            if {"" == $username} { set username $email}
            db_dml insert_user "
                insert into users (
                user_id, username
                ) values (
                :user_id, :username
                )
            "
            # Convert the person into a user
            db_dml party2user "
                update acs_objects
                set object_type = 'user'
                where object_id = :user_id
            "
        }
    
        person::update \
            -person_id $user_id \
            -first_names $first_names \
            -last_name $last_name
    
        party::update \
            -party_id $user_id \
            -url $url \
            -email $email
    
        acs_user::update \
            -user_id $user_id \
            -screen_name $screen_name \
            -username $username
    }
    
    # For all users (new and existing one):
    # Add a users_contact record to the user since the 3.0 PostgreSQL
    # port, because we have dropped the outer join with it...


    if {![db_string exists_p "select 1 from cc_users where user_id = :user_id" -default 0]} {
        db_dml add_users_contact "insert into users_contact (user_id) values (:user_id)"
    }
    
    if {[im_column_exists users_contact skype_screen_name]} {
        db_dml update_contact "update users_contact set cell_phone = :mobile_phone, skype_screen_name = :skype where user_id = :user_id"
    } else {
        db_dml update_contact "update users_contact set cell_phone = :mobile_phone where user_id = :user_id"
    }

    if {[im_column_exists persons proz_url]} {
        db_dml update_persons "update persons set proz_url = :proz_url where person_id = :user_id"
    }


    
    # Add the user to the "Registered Users" group, because
    # (s)he would get strange problems otherwise
    set registered_users [db_string registered_users "select object_id from acs_magic_objects where name='registered_users'"]
    set reg_users_rel_exists_p [db_string member_of_reg_users "
            select  count(*)
            from    group_member_map m, membership_rels mr
            where   m.member_id = :user_id
                and m.group_id = :registered_users
                and m.rel_id = mr.rel_id
                and m.container_id = m.group_id
                and m.rel_type::text = 'membership_rel'::text
    "]
    if {!$reg_users_rel_exists_p} {
        relation_add -member_state "approved" "membership_rel" $registered_users $user_id
    }
    
    
    # Add the user to a group.
    # Check whether the rest_user_id has the right to add the guy to the group:
    set managable_profiles [im_profile::profile_options_managable_for_user $rest_user_id]
    foreach profile_tuple $managable_profiles {
        set profile_name [lindex $profile_tuple 0]
        set profile_id [lindex $profile_tuple 1]
        if {$profile == $profile_id} { im_profile::add_member -profile_id $profile -user_id $user_id }
    }
    
    # TSearch2: We need to update "persons" in order to trigger the TSearch2
    # triggers
    db_dml update_persons "
        update persons
        set first_names = first_names
        where person_id = :user_id
    "
    
    
    # -----------------------------------------------------------------
    # Create a new Company if it didn't exist yet
    # -----------------------------------------------------------------
    set new_company_created_p 0
    # Double-Click protection: the company Id was generated at the new.tcl page
    if {0 == $company_exists_p} {
    
        db_transaction {
            # First create a new main_office:
            set main_office_id [im_office::new \
                -office_name    $office_name \
                -company_id     $company_id \
                -office_type_id [im_office_type_main] \
                -office_status_id   [im_office_status_active] \
                -office_path    $office_path]
    
            # add users to the office as
            set role_id [im_biz_object_role_office_admin]
            im_biz_object_add_role $user_id $main_office_id $role_id
    
            # Now create the company with the new main_office:
            set company_id [im_company::new \
                -company_id     $company_id \
                -company_name       $company_name \
                -company_path       $company_path \
                -main_office_id     $main_office_id \
                -company_type_id    $company_type_id \
                -company_status_id  $company_status_id \
                -no_callback]

            set new_company_created_p 1
        }

        # add users to the company as key account
        db_dml update_primary_contact "update im_companies set primary_contact_id = :user_id where company_id = :company_id and primary_contact_id is null"
        db_dml update_manager "update im_companies set manager_id = :rest_user_id where company_id = :company_id and manager_id is null"
    } 
    
    set role_id [im_biz_object_role_full_member]
    im_biz_object_add_role $user_id $company_id $role_id

    # -----------------------------------------------------------------
    # Update the Office
    # -----------------------------------------------------------------
    
    db_dml office_update "update im_offices set address_line1 = :street, address_city=:city, address_postal_code = :zipcode, address_country_code = :country, fax = :fax, phone = :phone where office_id = :main_office_id"
    
    
    # -----------------------------------------------------------------
    # Update the Company
    # -----------------------------------------------------------------
    set company_update_sql [list]
    foreach company_column [list paypal_email skrill_email vat_number iban bic] {
    
    if {[im_column_exists im_companies $company_column]} {
        switch $company_column {
        paypal_email {
            lappend company_update_sql "paypal_email = :paypal"
        }
        skrill_email {
            lappend company_update_sql "skrill_email = :skrill"
        }
        default {
            lappend company_update_sql "$company_column = :$company_column"
        }
        }
    }
    }
    
    # Run the update script
    if {[llength $company_update_sql] >0} {
    db_dml update_company "update im_companies set [join $company_update_sql ", "] where company_id = :company_id"
    }

    if {$new_company_created_p} {

        # ---------------------------------------------------------------
        # Add the default templates
        # ---------------------------------------------------------------
        set default_invoice_template_id [db_string invoice_template "select category_id from im_categories
            where category_type = 'Intranet Cost Template'
            and aux_int1 = 3700
            and enabled_p = 't'
            order by sort_order asc, category_id desc
            limit 1" -default ""]
        set default_bill_template_id [db_string invoice_template "select category_id from im_categories
            where category_type = 'Intranet Cost Template'
            and aux_int1 = 3704
            and enabled_p = 't'
            order by sort_order asc, category_id desc
            limit 1" -default ""]
        set default_po_template_id [db_string invoice_template "select category_id from im_categories
            where category_type = 'Intranet Cost Template'
            and aux_int1 = 3706
            and enabled_p = 't'
            order by sort_order asc, category_id desc
            limit 1" -default ""]
        set default_quote_template_id [db_string invoice_template "select category_id from im_categories
            where category_type = 'Intranet Cost Template'
            and aux_int1 = 3702
            and enabled_p = 't'
            order by sort_order asc, category_id desc
            limit 1" -default ""]

        db_dml update_default_templates "update im_companies
            set default_invoice_template_id = :default_invoice_template_id,
                default_bill_template_id = :default_bill_template_id,
                default_po_template_id = :default_po_template_id,
                default_quote_template_id = :default_quote_template_id
           where company_id = :company_id"
    

        
         # -----------------------------------------------------------------
        # Make sure the creator and the manager become Key Accounts
        # -----------------------------------------------------------------
    
        set role_id [im_company_role_key_account]
    
        im_biz_object_add_role $rest_user_id $company_id $role_id
        if {[exists_and_not_null manager_id]} {
            im_biz_object_add_role $manager_id $company_id $role_id
        }
    }
    
    # Mark the update in the dynfields
    cog::callback::invoke -object_type "im_office" -object_id $main_office_id -type_id [im_office_type_main] -status_id [im_office_status_active] -action after_create
    cog::callback::invoke -object_type "im_company" -object_id $company_id -type_id $company_type_id -status_id $company_status_id -action after_create
    
    
    set hash_array(rest_oid) "$company_id"
    set hash_array(created_user_id) "$user_id"
    set hash_arry(company_id) "$company_id"
    set hash_arry(main_office_id) "$main_office_id"
    return [array get hash_array]
}


ad_proc -public im_rest_post_object_type_sencha_email {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list subject body to_addr]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }
    
    set optional_vars [list from_addr upload_file company_id]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }

    if {$from_addr eq ""} {
        set from_addr [cc_email_from_party $rest_user_id]
    }

    # Insert the uploaded file linked under the package_id
    set package_id [apm_package_id_from_key "intranet-sencha-tables"]
    
    intranet_chilkat::send_mail \
        -to_addr $to_addr \
        -from_addr "$from_addr" \
        -subject "$subject" \
        -body "$body" \
        -object_id $company_id  \
        -use_sender

    set hash_array(rest_oid) "$company_id"
    set hash_array(company_id) "$company_id"

    return [array get hash_array]
}

ad_proc -public im_rest_post_object_type_sencha_contact_again {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list company_id contact_again_date]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }
    
    db_dml update_company "update im_companies set contact_again_date = to_date(:contact_again_date,'DD.MM.YYYY') where company_id = :company_id"

    set hash_array(rest_oid) "$company_id"
    set hash_array(company_id) "$company_id"

    return [array get hash_array]
}

ad_proc -public im_rest_post_object_type_sencha_company_status {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list company_id company_status_id]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }

    db_dml update_company "update im_companies set company_status_id = :company_status_id where company_id = :company_id"

    set hash_array(rest_oid) "$company_id"
    set hash_array(company_id) "$company_id"

    return [array get hash_array]
}


ad_proc -public im_rest_post_object_type_sencha_note {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list company_id note]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }

    set optional_vars [list note_id note_type_id]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }
    
    set note [string trim $note]
    
    if {$note_id eq ""} {
        set note [template::util::richtext::create $note "text/html" ]
        set duplicate_note_sql "select  count(*)
            from    im_notes
            where   object_id = :company_id and note = :note
        "
    
        if {[db_string dup $duplicate_note_sql]} {
            im_rest_error -format json -http_status 403 -message "The note already exists for company_id $company_id"
        }
    
        if {$note_type_id eq ""} {set note_type_id [im_note_type_other]}
    
        set note_id [db_exec_plsql create_note "
            SELECT im_note__new(
                NULL,
                'im_note',
                now(),
                :rest_user_id,
                '[ad_conn peeraddr]',
                null,
                :note,
                :company_id,
                :note_type_id,
                [im_note_status_active]
            )
        "]
    } else {
        ns_log Debug "Need updateing the node $note_id"
    }

    set hash_array(rest_oid) "$note_id"
    set hash_array(note_id) "$note_id"

    return [array get hash_array]
}

ad_proc -public im_rest_post_error {
    -error_list
    {-message "Error in Fields"}
} {
    Return the JSON with the errors for each field. The error_list is usually appended when we do the checking
    
    @error_list List of "field" & "error messages" for each field that has been throwing an error
} {

    set obj_ctr 0
    set errors ""
    foreach error $error_list {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append errors "$komma{\"element\": \"[im_quotejson [lindex $error 0]]\", \"error\": \"[im_quotejson [lindex $error 1]]\"}"
        incr obj_ctr
        
    }
    set result "{\"success\": false,\n\"message\": \"[im_quotejson $message]\",\n\"errors\": \[\n$errors\n\]\n}"
}

ad_proc -public im_rest_post_object_type_sencha_freelancer {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on freelancer DB
} {

    set success 1
    set error ""

    array set query_hash [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]
    
    set random_errors_token "[ad_generate_random_string]"

    # Setting 'required' variables
    #set required_vars [list first_names last_name email email company_type_id source_language_ids target_languages_ids country]
    set required_vars [list first_names last_name email company_type_id country source_language_ids target_language_ids]
    foreach var $required_vars {
        if {![exists_and_not_null query_hash($var)]} {
            sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "this is required field"
        } else {
            set $var $query_hash($var)
        }
    }

    # Setting 'optional' variables
    set optional_vars [list freelancer_id company_name telephone address zipcode city cert_source_language_ids cert_target_language_ids subject_area_ids note submitted_prices]
    foreach var $optional_vars {
        if {![info exists query_hash($var)]} {
            set $var ""
        } else {
            set $var $query_hash($var)
        }
    }

    set include_prices_p 0

   

    if {$submitted_prices ne ""} {
        set include_prices_p 1
    }

    sencha_errors::display_errors_and_warnings -object_id $random_errors_token

    if {$company_name eq ""} {
        set company_name "$first_names $last_name"
    }

    # skill type ids for both target and source language
    set source_language_skill_type_id [im_freelance_skill_type_source_language]
    set target_language_skill_type_id [im_freelance_skill_type_target_language]
    set subject_area_skill_type_id [im_freelance_skill_type_subject_area]
            
    # we need to buil lists, because frontend sends languages as string (comma seperated language ids)
    set source_languages_list_ids [list]
    set target_languages_list_ids [list]
    set cert_source_language_list_ids [list]
    set cert_target_language_list_ids [list]
    set subject_area_list_ids [list]

    foreach source_language_id [split $source_language_ids ","] {
        lappend source_languages_list_ids $source_language_id
    }

    foreach target_language_id [split $target_language_ids ","] {
        lappend target_languages_list_ids $target_language_id
    }

    foreach cert_source_language_id [split $cert_source_language_ids ","] {
        lappend cert_source_language_list_ids $cert_source_language_id
    }

    foreach cert_target_language_id [split $cert_target_language_ids ","] {
        lappend cert_target_language_list_ids $cert_target_language_id
    }

    foreach subject_area_id [split $subject_area_ids ","] {
        lappend subject_area_list_ids $subject_area_id
    }

    set unconfirmed_experience 2200
    set certified_experience 2204
    set added_new_freelancer_p 0

    if {$freelancer_id eq 0} {

        set freelancer_exist_p 0
        set company_exists_p 0

        set freelancer_id_check [db_string first_last_name_exists_p "
        select  user_id as freelancer_id_check
        from    cc_users
        where   lower(trim(email)) = lower(trim(:email))
        " -default 0]

        if {$freelancer_id_check == 0} {
            set $freelancer_exist_p 0
        } else {
            set error "Such email already exists"
            set email_exists_redirect_url "[ad_url][apm_package_url_from_key "intranet-core"]users/view?user_id=$freelancer_id_check"
            set result "{\"success\":false , \"error\": \"$error\", \"redirect_url\":\"[im_quotejson $email_exists_redirect_url]\", \"error_type\":\"user_exists\", \"existing_user_email\":\"$email\"}"
            im_rest_doc_return 200 "application/json" $result
            return
        }

        # taking care of users table

        if {$freelancer_exist_p == 0} {

           # creating new user
            set username [string tolower $email]
            set screen_name "$first_names $last_name"
            set password [ad_generate_random_string]
            set password_confirm $password

            array set creation_info [auth::create_user \
                     -verify_password_confirm \
                     -username $username \
                     -email $email \
                     -first_names $first_names \
                     -last_name $last_name \
                     -screen_name $screen_name \
                     -password $password \
                     -password_confirm $password_confirm \
                     ]
            set creation_status $creation_info(creation_status)
        

            if {[info exists creation_info(user_id)]} {
                set added_new_freelancer_p 1
                set user_id $creation_info(user_id)
                set freelancer_id $user_id
                set profile_id [im_profile_freelancers]
                im_profile::add_member -profile_id $profile_id -user_id $user_id
            } else {
                set user_id 0
            }

            if {$user_id ne 0} {
                set registered_users [db_string registered_users "select object_id from acs_magic_objects where name='registered_users'"]
                set reg_users_rel_exists_p [db_string member_of_reg_users "
                    select  count(*)
                    from    group_member_map m, membership_rels mr
                    where   m.member_id = :user_id
                        and m.group_id = :registered_users
                        and m.rel_id = mr.rel_id
                        and m.container_id = m.group_id
                        and m.rel_type::text = 'membership_rel'::text
               "]
                if {!$reg_users_rel_exists_p} {
                    relation_add -member_state "approved" "membership_rel" $registered_users $user_id
                }

                # we create two helping lists (for each langauge type), so that we make sure same language isn't added twice
                set already_added_source_languages [list]
                set already_added_target_languages [list]

                # adding certified source languages
                foreach cert_source_language_id $cert_source_language_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :cert_source_language_id, :source_language_skill_type_id, :certified_experience)"
                    if {[lsearch $already_added_source_languages $cert_source_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_source_languages $cert_source_language_id
                }

                # adding certified target languages
                foreach cert_target_language_id $cert_target_language_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :cert_target_language_id, :target_language_skill_type_id, :certified_experience)"
                    if {[lsearch $already_added_target_languages $cert_target_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_target_languages $cert_target_language_id
                }

                # adding source languages
                foreach source_language_id $source_languages_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :source_language_id, :source_language_skill_type_id, :unconfirmed_experience)"
                    if {[lsearch $already_added_source_languages $source_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_source_languages $source_language_id
                }

                # adding target languages
                foreach target_language_id $target_languages_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :target_language_id, :target_language_skill_type_id, :unconfirmed_experience)"
                    if {[lsearch $already_added_target_languages $target_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_target_languages $target_language_id
                }

                # adding subject area skill ids
                foreach subject_area_id $subject_area_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :subject_area_id, :subject_area_skill_type_id, :unconfirmed_experience)"
                    db_dml insert_freelance_skills $sql
                }

                # Taking care of Company of freelancer
                regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
    
                if {$company_exists_p eq 0} {
                    # Check if we have the path
                    set company_id [db_string company_exists "select company_id from im_companies where company_path = :company_path" -default 0]
                    if {$company_id ne 0} {
                        set company_exists_p 1
                   }
                }

                if {$company_exists_p eq 0} {
                    # default company status
                    set company_status_id [im_company_status_active]
                    set company_type_id [im_company_type_provider]

                    db_transaction {

                        set company_id [im_company::new \
                            -company_name       $company_name \
                            -company_type_id    $company_type_id \
                            -company_status_id  $company_status_id \
                            -no_callback ]

                        # nowt we create ne office
                        set office_path "${company_path}_main_office"
                        set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}]"
                        set office_type_id [im_office_type_main]        

                        set main_office_id [im_office::new \
                            -office_name    $office_name \
                            -company_id     $company_id \
                            -office_type_id [im_office_type_main] \
                            -office_status_id   [im_office_status_active] \
                            -office_path    $office_path]


                        # udpate office addresses
                        db_dml update_office_addresses "update im_offices set contact_person_id= :user_id, phone =:telephone, address_line1 =:address, address_postal_code =:zipcode, address_country_code =:country, address_city =:city where office_id =:main_office_id"

                        # add users to the office as
                        set role_id [im_biz_object_role_office_admin]
                        im_biz_object_add_role $user_id $main_office_id $role_id

                        # saving rest_user_id as key account
                        set key_account_role_id [im_company_role_key_account]
                        im_biz_object_add_role $rest_user_id $company_id $key_account_role_id
                    
                    }
                    set company_exists_p 1
                } 

                # setting new user as accounting contact & primary_contact + adding him as company member
                db_dml update_primary_contact "update im_companies set primary_contact_id = :user_id where company_id = :company_id"
                set role_id [im_biz_object_role_full_member]
                im_biz_object_add_role $user_id $company_id $role_id
    
            }

        }
        # Taking care of Note saving
        set save_note_p 1
        set default_note_type_id [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "DefaultNoteTypeId" -default ""]
        if {$default_note_type_id eq ""} {
            # we retrieve "other" note type id
            set default_note_type_id [db_string default_note_type_id "select category_id as default_note_type_id from im_categories where category_type ='Intranet Notes Type' and category = 'Other'" -default ""]
            # if it still equals empty string, then we do not save note at all
            if {$default_note_type_id eq ""} {
                set save_note_p 0
            }
        }
        if {$save_note_p eq 1} {
            set note [template::util::richtext::create $note "text/html"]
            set note_id [db_exec_plsql create_note "
                SELECT im_note__new(
                    NULL,
                    'im_note',
                    now(),
                    :rest_user_id,
                    '[ad_conn peeraddr]',
                    null,
                    :note,
                    :user_id,
                    :default_note_type_id,
                    [im_note_status_active]
                    )
            "]
        }
        # taking care of prices
        if {$include_prices_p == 1 && $company_exists_p == 1} {
            set arr [lindex $submitted_prices 1]
            set currency "EUR"
            set valid_from ""
            set valid_through ""
            set subject_area_id ""
            set file_type_id ""
            set note ""
            for {set i 0} {$i < [llength $arr]} {incr i} {
                array set price_arr [lindex [lindex $arr $i] 1]
                set price_id [db_nextval "im_trans_prices_seq"]
                set uom_id $price_arr(uom_id)
                set task_type_id $price_arr(task_type_id)
                set price_source_language_id $price_arr(source_language_id)
                set price_target_language_id $price_arr(target_language_id)
                set price $price_arr(price)
                set min_price $price_arr(min_price)


                set sql "
                insert into im_trans_prices (price_id, uom_id, company_id, task_type_id, source_language_id, target_language_id, subject_area_id, file_type_id, valid_from, valid_through, currency, price, min_price, note) 
                values (:price_id, :uom_id, :company_id, :task_type_id, :price_source_language_id, :price_target_language_id, :subject_area_id, :file_type_id, :valid_from, :valid_through, :currency, :price, :min_price, :note)"
                db_dml insert_freelance_skills $sql
            }
        }
    }
        
    if {$freelancer_id > 0 && $added_new_freelancer_p eq 0} {
        
        # updating user personal info
        # we need username
        db_1row freelancer_username "select username from cc_users where object_id=:freelancer_id"
        im_user_update_existing_user -user_id $freelancer_id -username $username -email $email -first_names $first_names -last_name $last_name

        # first we udpate company main office
        set freelancer_company_id [im_translation_freelance_company -freelance_id $freelancer_id]
        set company_data [db_0or1row freelancer_data "select company_name as old_company_name, main_office_id from im_companies where company_id =:freelancer_company_id"]
        set company_office_id $main_office_id
        db_dml update_company_main_office "update im_offices set address_line1 =:address, address_country_code=:country, address_postal_code=:zipcode, address_city=:city, phone=:telephone where office_id=:company_office_id"

        # updating user company info
        db_dml update_freelancer_company "update im_companies set company_name=:company_name where company_id=:freelancer_company_id"

        # updating user target and source languages
        # we first need to remove existing soutce and target languages + subject area skills
        db_dml delete_target_and_source_languages "delete from im_freelance_skills where user_id =:freelancer_id and skill_type_id =:source_language_skill_type_id"
        db_dml delete_target_and_source_languages "delete from im_freelance_skills where user_id =:freelancer_id and skill_type_id =:target_language_skill_type_id"
        db_dml delete_target_and_source_languages "delete from im_freelance_skills where user_id =:freelancer_id and skill_type_id =:subject_area_skill_type_id"

        # we create two helping lists (for each langauge type), so that we make sure same language isn't added twice
        set already_added_source_languages [list]
        set already_added_target_languages [list]

        # adding certified source languages
        foreach cert_source_language_id $cert_source_language_list_ids {
            set sql "
                insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                values (:freelancer_id, :cert_source_language_id, :source_language_skill_type_id, :certified_experience)"
            if {[lsearch $already_added_source_languages $cert_source_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_source_languages $cert_source_language_id
        }

        # adding certified target languages
            foreach cert_target_language_id $cert_target_language_list_ids {
                set sql "
                    insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                    values (:freelancer_id, :cert_target_language_id, :target_language_skill_type_id, :certified_experience)"
            if {[lsearch $already_added_target_languages $cert_target_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_target_languages $cert_target_language_id
        }

        # adding source languages
        foreach source_language_id $source_languages_list_ids {
            set sql "
            insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
            values (:freelancer_id, :source_language_id, :source_language_skill_type_id, :unconfirmed_experience)"
            if {[lsearch $already_added_source_languages $source_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_source_languages $source_language_id
        }

        # adding target languages
        foreach target_language_id $target_languages_list_ids {
            set sql "
            insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
            values (:freelancer_id, :target_language_id, :target_language_skill_type_id, :unconfirmed_experience)"
            if {[lsearch $already_added_target_languages $target_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_target_languages $target_language_id
        }
        
        # adding subject area skills
        foreach subject_area_id $subject_area_list_ids {
            set sql "
            insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
            values (:freelancer_id, :subject_area_id, :subject_area_skill_type_id,:unconfirmed_experience)"
            db_dml insert_freelance_skills $sql
        }

        # Taking care of notes when saving
        # First we check if submitted note has any content
        if {[string length $note] > 0} {
            set save_note_p 1
        } else {
            set save_note_p 0
        } 

         # Then if note has some content, we need to make sure that we actually have 'DefaultNoteType' set up in system
         # Possible change: so far it always proceeds, because if no 'DefaultNoteType' is found, we use 'Other' as type
        if {$save_note_p eq 1} {
            set default_note_type_id [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "DefaultNoteTypeId" -default ""]
            if {$default_note_type_id eq ""} {
                # we retrieve "other" note type id
                set default_note_type_id [db_string default_note_type_id "select category_id as default_note_type_id from im_categories where category_type ='Intranet Notes Type' and category = 'Other'" -default ""]
                # if it still equals empty string, then we do not save note at all
                if {$default_note_type_id eq ""} {
                    set save_note_p 0
                }
            }

        }

        # if NoteType exist, then we check if freelaner alraedy has such note
        if {$save_note_p eq 1} {
            set note [template::util::richtext::create $note "text/html"]
            set count_freelancer_notes [db_string count_freelancer_notes "select count(*) from im_notes where object_id = :freelancer_id and note_type_id=:default_note_type_id" -default 0]
            # if no notes of that type for that FL exist, we create a new one
            if {$count_freelancer_notes == 0} {
                set note_id [db_exec_plsql create_note "
                    SELECT im_note__new(
                        NULL,
                        'im_note',
                        now(),
                        :rest_user_id,
                        '[ad_conn peeraddr]',
                        null,
                        :note,
                        :freelancer_id,
                        :default_note_type_id,
                        [im_note_status_active]
                    )"
                ]
            } else {
                # such note already exist, getting its ID
                db_0or1row existing_note_id "select note_id as existing_note_id from im_notes where object_id = :freelancer_id and note_type_id=:default_note_type_id"
                if {$existing_note_id} {
                    db_dml update_exsting_note "update im_notes set note =:note where note_id =:existing_note_id"
                }
            }
        }

        # taking care of prices
        if {$include_prices_p} {
            db_dml delete_freelancer_prices "delete from im_trans_prices where company_id =:freelancer_company_id"
            set arr [lindex $submitted_prices 1]
            set currency "EUR"
            set valid_from ""
            set valid_through ""
            set subject_area_id ""
            set file_type_id ""
            set note ""
            for {set i 0} {$i < [llength $arr]} {incr i} {
                array set price_arr [lindex [lindex $arr $i] 1]
                set price_id [db_nextval "im_trans_prices_seq"]
                set uom_id $price_arr(uom_id)
                set task_type_id $price_arr(task_type_id)
                set price_source_language_id $price_arr(source_language_id)
                set price_target_language_id $price_arr(target_language_id)
                set price $price_arr(price)
                set min_price $price_arr(min_price)


                set sql "
                insert into im_trans_prices (price_id, uom_id, company_id, task_type_id, source_language_id, target_language_id, subject_area_id, file_type_id, valid_from, valid_through, currency, price, min_price, note) 
                values (:price_id, :uom_id, :freelancer_company_id, :task_type_id, :price_source_language_id, :price_target_language_id, :subject_area_id, :file_type_id, :valid_from, :valid_through, :currency, :price, :min_price, :note)"
                db_dml insert_freelance_skills $sql
            }
        }

        set user_id $freelancer_id

    }


    im_rest_get_custom_sencha_single_freelancer -query_hash_pairs [list freelancer_id $freelancer_id]

    #set result "{\"success\":true , \"error\": \"$error\"}"
    #im_rest_doc_return 200 "application/json" $result
    #return

    set hash_array(rest_oid) $freelancer_id
        
    return [array get hash_array]

}


