set user_id [ad_maybe_redirect_for_registration]

set CompanyCreationModalDisplayOn [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "CompanyCreationModalDisplayOn" -default "button"]

set default_project_type_value [im_dynfield::attribute::get_default_value -attribute_id 307280]
set retrieveFromCompanyDefaults [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "RetrieveFromCompanyDefaults" -default false]

template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "/sencha-tables/projects.js"

set source_words_column_hidden_by_default_p [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "SourceWordsColumnHiddenByDefaultP" -default 1]

set project_with_analysis_button_visible_p 0
set memoq_configured_p 0
set trados_configured_p 0

if {[llength [namespace which im_trans_memoq_configured_p]]} {
	if {[im_trans_memoq_configured_p]} {
		set memoq_configured_p 1
	}
}

if {[llength [namespace which im_trans_trados_configured_p]]} {
	if {[im_trans_trados_configured_p]} {
		set trados_configured_p 1
	}
}

if {$trados_configured_p || $memoq_configured_p} {
	set project_with_analysis_button_visible_p 1
}


template::head::add_javascript -script "
	var serverParams = {
	   	serverUrl: '[ad_url]',
	   	defaultProjectTypeValue: '$default_project_type_value',
	   	retrieveFromCompanyDefaults:$retrieveFromCompanyDefaults,
	   	sourceWordsColumnHiddenByDeafult:$source_words_column_hidden_by_default_p,
	   	projectWithAnalysisButtonVisibleP:$project_with_analysis_button_visible_p
	};
	serverParams.restUrl = serverParams.serverUrl + '/cognovis-rest/';
	serverParams.CompanyCreationModalDisplayOn = '$CompanyCreationModalDisplayOn'
	serverParams.auth = '';
"

set counter 0
set storefilter_js_list ""
set projectsStoreFilter_js "
   var projectsStoreFilter = \{
	   total: $counter,
	   data: \[
	   		[join $storefilter_js_list ", \n"]
	   \]
   \};
"
template::head::add_javascript -script $projectsStoreFilter_js

# Required form fields
set project_form_fields [list project_name company_id customer_contact_id project_type_id language_experience_level_id source_language_id \
	selectorFilter target_language_ids subject_area_id]

set optional_project_fields [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "ProjectCreationOptionalFields" -default ""]

foreach field $optional_project_fields {
	lappend project_form_fields $field
}

set project_form_json "\"[join $project_form_fields "\",\""]\""

template::head::add_javascript -script "serverParams.projectFormDisplayedFields = \[ $project_form_json \];"

set company_form_fields [list company_type_id company_id country first_names last_name email street zipcode city]
set company_form_json "\"[join $company_form_fields "\",\""]\""

template::head::add_javascript -script "serverParams.companyFormDisplayedFields = \[ $company_form_json \];"

set focus_field [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "ProjectCreationFocusField" -default ""]
template::head::add_javascript -script "serverParams.focus_field = '$focus_field';"

if {[exists_and_not_null company_id]} {
	im_company_permissions $user_id $company_id view read write admin
}

set filter_js_list [list]
set counter 0
foreach v [list project_status_id project_lead_id project_type_id company_id freelancer_id] {
	if {[exists_and_not_null $v]} {
		incr counter
		lappend filter_js_list "
			\{
			   	name: '$v',
			   	values: \[ [set $v] \]
		   \}
		"
	}
}


set projectsFilter_js "
   var projectsFilter = \{
	   total: $counter,
	   data: \[
	   		[join $filter_js_list ", \n"]
	   \]
   \};
"

template::head::add_javascript -script $projectsFilter_js

if {![info exists global_hide_columns]} {
	set global_hide_columns ""
}

template::head::add_javascript -script "var projectsHideColumn = '$global_hide_columns';"

if {[exists_and_not_null customer_id]} {
	
	# Get the most often used project type for the customer
	set customer_project_type_id [db_string project_type "select project_type_id from (select project_type_id, count(project_type_id) as count from im_projects where company_id = :customer_id group by project_type_id) c order by count desc limit 1" -default ""]
	set customer_source_language_id [db_string project_type "select source_language_id from (select source_language_id, count(source_language_id) as count from im_projects where company_id = :customer_id group by source_language_id) c order by count desc limit 1" -default ""]
	set customer_subject_area_id [db_string project_type "select subject_area_id from (select subject_area_id, count(subject_area_id) as count from im_projects where company_id = :customer_id group by subject_area_id) c order by count desc limit 1" -default ""]
	set customer_final_company_id [db_string project_type "select final_company_id from (select final_company_id, count(final_company_id) as count from im_projects where company_id = :customer_id group by final_company_id) c order by count desc limit 1" -default ""]
	
	foreach var [list customer_project_type_id customer_source_language_id customer_subject_area_id customer_final_company_id] {
		if {[set $var] eq ""} {set $var "''"}
	}
	template::head::add_javascript -script "
	var newProjectData = \{
		total: 5,
		data: \[
			\{
				name: 'customer_id',
				value: $customer_id
			\},
			\{
				name: 'project_type_id',
				value: $customer_project_type_id
			\},
			\{
				name: 'source_language_id',
				value: $customer_source_language_id
			\},
			\{
				name: 'subject_area_id',
				value: $customer_subject_area_id
			\},
			\{
				name: 'final_company_id',
				value: $customer_final_company_id
			\},
			\{
				name: 'show_edit_p',
				value: 1
			\}
		\]
	\};"
}
