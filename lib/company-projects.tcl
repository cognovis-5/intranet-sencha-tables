set user_id [ad_maybe_redirect_for_registration]


template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"


im_company_permissions $user_id $company_id view read write admin
template::head::add_javascript -src "/sencha-tables/projects.js"

template::head::add_javascript -script "
	var serverParams = {
	   	serverUrl: '[ad_url]',
	};
	serverParams.restUrl = serverParams.serverUrl + '/cognovis-rest/';
	serverParams.auth = '';
"

set storefilter_js_list ""

foreach v [list company_id freelancer_id] {
	if {[exists_and_not_null $v]} {
		incr counter
		lappend storefilter_js_list "
			\{
			   	name: '$v',
			   	values: \[ [set $v] \]
		   \}
		"
	}
}

set projectsStoreFilter_js "
   var projectsStoreFilter = \{
	   total: $counter,
	   data: \[
	   		[join $storefilter_js_list ", \n"]
	   \]
   \};
"

template::head::add_javascript -script $projectsStoreFilter_js

if {[exists_and_not_null company_id]} {
	im_company_permissions $user_id $company_id view read write admin
}

set filter_js_list [list]
set counter 0
foreach v [list project_status_id project_lead_id project_type_id company_id] {
	if {[exists_and_not_null $v]} {
		incr counter
		lappend filter_js_list "
			\{
			   	name: '$v',
			   	values: \[ [set $v] \]
		   \}
		"
	}
}

if {[info exists global_hide_columns]} {
	template::head::add_javascript -script "var globalHideColumn = '$global_hide_columns';"
}

set projectsFilter_js "
   var projectsFilter = \{
	   total: $counter,
	   data: \[
	   		[join $filter_js_list ", \n"]
	   \]
   \};
"

template::head::add_javascript -script $projectsFilter_js
if {![info exists global_hide_columns]} {
	set global_hide_columns ""
}

template::head::add_javascript -script "var projectsHideColumn = '$global_hide_columns';"


# Required form fields
set project_form_fields [list project_name company_id customer_contact_id project_type_id source_language_id \
	selectorFilter target_language_ids subject_area_id]

set optional_project_fields [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "ProjectCreationOptionalFields" -default ""]

foreach field $optional_project_fields {
	lappend project_form_fields $field
}

set project_form_json "\"[join $project_form_fields "\",\""]\""

template::head::add_javascript -script "serverParams.projectFormDisplayedFields = \[ $project_form_json \];"

set company_form_fields [list company_type_id company_id country first_name last_name email]
set company_form_json "\"[join $company_form_fields "\",\""]\""

template::head::add_javascript -script "serverParams.companyFormDisplayedFields = \[ $company_form_json \];"