# /packages/intranet-filestorage/www/download/download.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.


ad_page_contract {
    Download a file
    @author frank.bergmann@project-open.com
} {
    rfq_file_id:notnull
}  -errors {
    rfq_file_id:notnull {No rfq file id was specified}
}

set user_id [ad_maybe_redirect_for_registration]

db_1row get_file_data "select * from external_rfq_files where rfq_file_id =:rfq_file_id" 



set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
set external_uploads_folder "$base_path_unix/external_rfq"
set file "$external_uploads_folder/$filename"
set guessed_file_type [ns_guesstype $file]

if [file readable $file] {
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=$original_filename"
    ns_returnfile 200 $guessed_file_type $file
} else {
    ad_returnredirect "/error.tcl"
}
