ad_page_contract {
    Shows all companies. Lots of dimensional sliders

    @param status_id if specified, limits view to those of this status
    @param type_id   if specified, limits view to those of this type
} {
    { company_status_id:integer "[im_company_status_active]" }
    { company_type_id:integer "[im_company_type_customer]" }
}

set user_id [ad_maybe_redirect_for_registration]

template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "/sencha-tables/companies.js"

template::head::add_javascript -script "
    var serverParams = {
           serverUrl: '[ad_url]',
    };
    serverParams.restUrl = serverParams.serverUrl + '/cognovis-rest/';
    serverParams.auth = '';
"

set storeFilterList [list]
set global_hide_columns [list]
# If we have to contact as the status_id, make it a store filter
if {[exists_and_not_null company_status_id]} {
    if {$company_status_id == 4800} {
        lappend storeFilterList company_status_id
    } else {
        lappend global_hide_columns total_invoice_amount
        lappend global_hide_columns total_quote_amount
    }
}
set counter 0
set storefilter_js_list ""

foreach v $storeFilterList {
    if {[exists_and_not_null $v]} {
        incr counter
        lappend storefilter_js_list "
            \{
                   name: '$v',
                   values: \[ [set $v] \]
           \}
        "
    }
}

set companiesStoreFilter_js "
   var companiesStoreFilter = \{
       total: $counter,
       data: \[ 
               [join $storefilter_js_list ", \n"]
       \]
   \};
"

template::head::add_javascript -script $companiesStoreFilter_js

if {[exists_and_not_null company_id]} {
    im_company_permissions $user_id $company_id view read write admin
}

set filter_js_list [list]
set counter 0
foreach v [list company_status_id company_type_id company_id] {
    if {[exists_and_not_null $v]} {
        incr counter
        lappend filter_js_list "
            \{
                   name: '$v',
                   values: \[ [set $v] \]
           \}
        "
    }
}


set companiesFilter_js "
   var companiesFilter = \{
       total: $counter,
       data: \[ 
               [join $filter_js_list ", \n"]
       \]
   \};
"

template::head::add_javascript -script $companiesFilter_js

template::head::add_javascript -script "var companiesHideColumn = '[join $global_hide_columns ","]';"
