
ad_page_contract {
    Download a file
    @author michaldrn@wp.pl
} {
    project_id:notnull
}  -errors {
	project_id:notnull {No project_id was specified}
}
 
set project_nr [db_string project_info "select * from im_projects where project_id = :project_id" -default ""]

if {$project_nr ne ""} {

    set delivery_files_folder_name [parameter::get_from_package_key -package_key "sencha-portal" -parameter "DeliveryFolder" -default "Final"]


    set zip_dir "[im_filestorage_project_path $project_id]/$delivery_files_folder_name"

    set delivered_file_list [glob -nocomplain -directory "${zip_dir}" "*"]

    if {[llength $delivered_file_list] > 0} {

        set zipfile /tmp/folder_${project_nr}.zip
        intranet_chilkat::create_zip -directories "$zip_dir" -zipfile $zipfile

        # Return the file
        set outputheaders [ns_conn outputheaders]
        ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"delivery_files_${project_nr}.zip\""
        ns_returnfile 200 application/zip $zipfile
            
        exec rm -rf $zipfile

    } else {
        ad_return_error "No delivery files" "We could not find any delivery files within project $project_nr"
    }

} else {
   ad_return_error "Project not found" "Project with project_id $project_id does not exist"
}

