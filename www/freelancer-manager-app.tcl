ad_page_contract {
    @author ND
} {
    {target_language_id ""}
}

set user_id [ad_maybe_redirect_for_registration]
set project_id 0

set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]
set company_status_ids_comma_seperated [join $company_status_ids ","]

set previous_url [get_referrer]
set quality_column_hidden_by_default_p [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "QualityColumnHiddenByDefaultP" -default 1]

template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "[apm_package_url_from_key "intranet-sencha-tables"]/freelancer-manager-app.js"
set package_url [ad_url][apm_package_url_from_key "intranet-sencha-tables"]
# im_company_permissions $user_id $company_id view read write admin

template::head::add_javascript -script "
       var serverParams = {
           serverUrl: '[ad_url]',
           packageUrl: '$package_url',
           user_id: '',
           auto_login: '',
           project_id: $project_id,
           freelancerAdderDefaultCompanyType:58,
           previousUrl:'$previous_url',
           qualityColumnHiddenByDeafult:$quality_column_hidden_by_default_p
       };
       serverParams.auth = '';
       serverParams.restUrl = serverParams.serverUrl + '/cognovis-rest/';"



set filter_js_list [list]
set counter 0

foreach v [list project_id skill_2002 skill_2000 skill_2014 skill_2024] {
    if {[exists_and_not_null $v]} {
        incr counter
        
        # Lists need to be transformed for the json to work
        if {[llength $v]>1} {
            set v [join $v ","]
        }
        lappend filter_js_list "
            \{
                   name: '$v',
                   values: \[ [set $v] \]
           \}
        "
    }
}

template::head::add_javascript -script "
    var defaultCompaniesStatus = '$company_status_ids_comma_seperated';
"

template::head::add_javascript -script "
    var filterParams = {
        project_id: '$project_id',
        skill_2002: '$target_language_id',
        minimumLanguageExperienceLevel:0,
        languageExperienceLevelFilterAlreadyUsed:false
    };
"
set projectFreelancersFilter_js "
   var projectFreelancersFilter = \{
       total: $counter,
       data: \[
               [join $filter_js_list ", \n"]
       \]
   \};"


template::head::add_javascript -script $projectFreelancersFilter_js
