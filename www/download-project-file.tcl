
ad_page_contract {
    Download a file
    @author michaldrn@wp.pl
} {
    project_id:notnull
    file_name:notnull
}  -errors {
	project_id:notnull {No project_id was specified}
	project_id:notnull {No file_name was specified}
}
 

set completed_files_folder_name [parameter::get_from_package_key -package_key "sencha-portal" -parameter "DeliveryFolder" -default "Final"]
set file "[im_filestorage_project_path $project_id]/$completed_files_folder_name/$file_name"

set file_type [file extension $file]

if [file readable $file] {
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=$file_name"
    ns_returnfile 200 $file_type $file
} else {
    ad_returnredirect "/error.tcl"
}
