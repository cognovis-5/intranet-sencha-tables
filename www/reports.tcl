ad_page_contract {
    @author ND
} {
    {group_by "project_type_id"}
}

set user_id [ad_maybe_redirect_for_registration]

# Only for senior managers
if {![im_profile::member_p -profile_id [im_profile_senior_managers] -user_id $user_id]} {
#	ad_return_error "Not allowed" "You are not allowed to view this page"
}

template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "[apm_package_url_from_key "intranet-sencha-tables"]/reports.js"
# im_company_permissions $user_id $company_id view read write admin


template::head::add_javascript -script "
	   var serverParams = {
       	serverUrl: '[ad_url]',
       	group_reports_by:'$group_by'
       };
       serverParams.restUrl = serverParams.serverUrl + '/cognovis-rest/';"

