SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.2.0.1.5-4.2.0.1.6.sql','');

SELECT im_grant_permission(195068,461,'view');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin
     ALTER TABLE external_rfq ADD COLUMN customer_reference_number varchar;
     ALTER TABLE external_rfq ADD COLUMN subject_area_id integer;

return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


DROP FUNCTION IF EXISTS external_rfq__new(
   p_first_names varchar, 
   p_last_name varchar, 
   p_cost_center_id integer,
   p_company_name varchar,
   p_email varchar,
   p_country_code varchar(2),
   p_telephone varchar,
   p_quality_level_id integer,
   p_project_type_id integer,
   p_source_language_id integer,
   p_target_language_ids varchar,
   p_matched_user_id integer,
   p_matched_company_id integer,
   p_comment varchar,
   p_creation_date timestamptz,
   p_deadline timestamptz
);



CREATE OR REPLACE FUNCTION external_rfq__new(
   p_first_names varchar, 
   p_last_name varchar, 
   p_cost_center_id integer,
   p_company_name varchar,
   p_email varchar,
   p_country_code varchar(2),
   p_telephone varchar,
   p_quality_level_id integer,
   p_project_type_id integer,
   p_source_language_id integer,
   p_target_language_ids varchar,
   p_matched_user_id integer,
   p_matched_company_id integer,
   p_comment varchar,
   p_creation_date timestamptz,
   p_deadline timestamptz,
   p_customer_reference_number varchar,
   p_subject_area_id integer

) RETURNS integer AS $$
declare
new_rfq_id integer;
BEGIN
    insert into external_rfq (first_names, last_name, cost_center_id, company_name, email, country_code, telephone, quality_level_id, project_type_id, source_language_id, target_language_ids, matched_user_id, matched_company_id, comment, creation_date, deadline, customer_reference_number, subject_area_id) 
        values (p_first_names, p_last_name, p_cost_center_id, p_company_name, p_email, p_country_code, p_telephone, p_quality_level_id, p_project_type_id, p_source_language_id, p_target_language_ids, p_matched_user_id, p_matched_company_id, p_comment, p_creation_date, p_deadline, p_customer_reference_number, p_subject_area_id)
        returning external_rfq.rfq_id into new_rfq_id;
    return new_rfq_id;
END;
$$ LANGUAGE plpgsql;