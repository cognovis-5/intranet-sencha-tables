SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.2.0.0.1-4.2.0.0.2.sql','');


SELECT  im_component_plugin__new (
        null,                           		-- plugin_id
        'acs_object',                			-- object_type
        now(),                        			-- creation_date
        null,                           		-- creation_user
        null,                           		-- creation_ip
        null,                           		-- context_id
        'Sencha RFQ', 		-- plugin_name
        'intranet-sencha-tables',            		-- package_name
        'top',                        		-- location
        '/intranet/index',      		-- page_url
        null,                           		-- view_name
        5,                              		-- sort_order
        'im_sencha_rfq_component'  	-- component_tcl
);


create or replace function inline_0 () returns integer as $body$
        DECLARE
                v_plugin_id     integer;
		v_employees	integer;
        BEGIN

	select  plugin_id
        into    v_plugin_id
        from    im_component_plugins pl
        where   plugin_name = 'Sencha RFQ';

	select group_id into v_employees from groups where group_name = 'Employees';
 
        PERFORM im_grant_permission(v_plugin_id, v_employees, 'read');

	return 1; 
		
        END;
$body$ language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

SELECT  im_component_plugin__new (
        null,                           		-- plugin_id
        'acs_object',                			-- object_type
        now(),                        			-- creation_date
        null,                           		-- creation_user
        null,                           		-- creation_ip
        null,                           		-- context_id
        'Sencha Projects', 		-- plugin_name
        'intranet-sencha-tables',            		-- package_name
        'top',                        		-- location
        '/intranet/index',      		-- page_url
        null,                           		-- view_name
        1,                              		-- sort_order
        'im_sencha_personal_projects_component -project_lead_id [ad_conn user_id] -project_status_id "[im_project_status_open],[im_project_status_potential]" -global_hide_columns "project_nr,project_lead_id,start_date,members,project_type_id, project_status_id,end_date"'  	-- component_tcl
);


create or replace function inline_0 () returns integer as $body$
        DECLARE
                v_plugin_id     integer;
		v_employees	integer;
        BEGIN

	select  plugin_id
        into    v_plugin_id
        from    im_component_plugins pl
        where   plugin_name = 'Sencha Projects';

	select group_id into v_employees from groups where group_name = 'Employees';
 
        PERFORM im_grant_permission(v_plugin_id, v_employees, 'read');

	return 1; 
		
        END;
$body$ language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();
