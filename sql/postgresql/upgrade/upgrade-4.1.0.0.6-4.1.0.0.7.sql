--
--
--
-- Copyright (c) 2017, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author <yourname> (<your email>)
-- @creation-date 2017-12-08
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.1.0.0.6-4.1.0.0.7.sql','');

-- Add the pages for intranet-rest
create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
    select count(*) into v_count from im_categories
        where category_id = 2028;
    if v_count > 0 then return 0; end if;
    INSERT INTO im_categories (category_id,category,category_type,aux_string1, aux_string2, enabled_p, parent_only_p)
VALUES (2028,''Language Experience Level'',''Intranet Skill Type'','''',''language_exp_level'',''t'',''f'');
    return 0;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();
