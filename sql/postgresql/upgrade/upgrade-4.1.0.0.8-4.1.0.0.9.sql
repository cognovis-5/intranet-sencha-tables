SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.1.0.0.8-4.1.0.0.9.sql','');


-- Update default project type values
create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
    update im_dynfield_type_attribute_map set default_value = ''tcl {im_next_project_nr}'' where attribute_id = 307276;
    return 0;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();
